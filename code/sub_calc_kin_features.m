function [correct, incorrect] = sub_calc_kin_features(data_subjects,this_feat,this_emo,feat,c3dData,orderEmotions)
%% init vars
subjects = fieldnames(data_subjects);
nSubj = numel(subjects);
correct = nan(nSubj,1);
incorrect = nan(nSubj,1);

%% loop subjects
for iSubj = 1:nSubj
    actual_subject = subjects{iSubj};
    
    % extract 'this_emo' stimuli
    stimuli = data_subjects.(actual_subject).stimCode(strcmp(data_subjects.(actual_subject).cat_t01,this_emo));
    nStim = numel(stimuli);
    actual_recog = false(nStim,4);
    actual_kin = nan(nStim,4);
    
    % loop subject's 'this_emo' stimuli
    for iStim = 1:nStim
        % get stimulus and index of stimulus within subject_specific results
        actual_stimulus = stimuli{iStim};
        idxStim = strcmp(data_subjects.(actual_subject).stimCode,actual_stimulus);
        
        % ******************* find recognition rate **************************************
        % rated emotions of shifted trials
        actual_ratings = {data_subjects.(actual_subject).cat_t02{idxStim},...
            data_subjects.(actual_subject).cat_t500{idxStim},...
            data_subjects.(actual_subject).cat_t1000{idxStim},...
            data_subjects.(actual_subject).cat_t2000{idxStim}};
        
        % comparing to baseline
        actual_recog(iStim,:) = ismember(actual_ratings, this_emo);
        
        % ******************* find MotionEnergy ******************************************
        % validated emotion of current stimulus
        actual_emo = data_subjects.(actual_subject).stimCat{idxStim};
        
        % get index of stimulus within feature_set
        if strcmp(actual_stimulus(3),'t')
            tmp_idx = find(strcmp({c3dData.file}', ['s0' actual_stimulus(2:end)  '_0ms.c3d']));
        else
            tmp_idx = find(strcmp({c3dData.file}', [actual_stimulus '_0ms.c3d']));
        end
        actual_idx = ceil((tmp_idx - (68 * (find(strcmp(orderEmotions,actual_emo))-1))) / 4);
        
        % get index of 'this_feature' within 'feat' variables
        idx_t0 = strcmp({feat.([actual_emo '_t0']).name},this_feat);
        idx_t500 = strcmp({feat.([actual_emo '_t500']).name},this_feat);
        idx_t1000 = strcmp({feat.([actual_emo '_t1000']).name},this_feat);
        idx_t2000 = strcmp({feat.([actual_emo '_t2000']).name},this_feat);
        
        % get specific kinematic feature
        actual_kin(iStim,1) = feat.([actual_emo '_t0'])(idx_t0).fSet(actual_idx);
        actual_kin(iStim,2) = feat.([actual_emo '_t500'])(idx_t500).fSet(actual_idx);
        actual_kin(iStim,3) = feat.([actual_emo '_t1000'])(idx_t1000).fSet(actual_idx);
        actual_kin(iStim,4) = feat.([actual_emo '_t2000'])(idx_t2000).fSet(actual_idx);
    end
    
    % calculate mean for correct and incorrect (vs baseline) rated trials
    correct(iSubj,1) = mean(actual_kin(actual_recog));
    incorrect(iSubj,1) = mean(actual_kin(~actual_recog));
    
end % subject loop