%% Analysis B: investigating influence of temporal offset on emotion recognition by means 
%              of comparing emotion category ratings of temporally decoupled sequences 
%              with individual baseline sequences
%
% code that leads to 
%   - figures 3 and 4 in the manuscript
%   - statistical values (see output in command window)
%
% used References:
% Girden, E. R. (1992). ANOVA: Repeated measures. Sage university papers. Quantitative
%       applications in the social sciences: no. 07-084. Newbury Park, Calif.: Sage Publications.
% Templeton, G.F. (2011). A Two-Step Approach for Transforming Continuous Variables to
%       Normal: Implications and Recommendations for IS Research. Communications of the AIS, Vol. 28, Article 4.
% Tomczak, M., & Tomczak, E. (2014). The need to report effect size estimates revisited. 
%       An overview of some recommended measures of effect size. Trends Sport Sci. 1(21), 1925.

clear all; close all;
disp(' *** Analysis B: investigating influence of temporal offset on emotion recognition');

%% options *******************************************************************************
saving_data = true;
saving_figs = true;

% folder
folder.data = fullfile('..','analysis_results');
folder.figs = fullfile('..','analysis_figures');

%% init vars *****************************************************************************
disp('     --> load raw experimental data');
[data_raw, subjects] = sub_load_txt_files(folder); % load experimental data

nSubj = numel(subjects);
nStim = 68;

% order of emotional categories and timeshifts
orderEmotions = {'Af','An','Ha','Sa'};
orderTimeShifts = {'t0','t500','t1000','t2000'};
legendCaption = 'temporal offset';

%% ****************** fetch relevant data ************************************************
disp('     --> calculate recognition rates/d''');
% loop subjects and fetch relevant data
for iSubj = 1:nSubj
    %% +++++++ PERFORMANCE +++++++++++++++++++++++++++++++++++++++++++++
    % here, get individual recognition rates for each time_shift, with respect to the first t0-trial
    actual_recog = zeros(nStim,4);
    for ts = 1:nStim
        % baseline emotions
        baseline_emotion = data_raw.(subjects{iSubj}).cat_t01{ts};
        
        % rated emotions of shifted trials
        actual_ratings = {data_raw.(subjects{iSubj}).cat_t02{ts},...
            data_raw.(subjects{iSubj}).cat_t500{ts},...
            data_raw.(subjects{iSubj}).cat_t1000{ts},...
            data_raw.(subjects{iSubj}).cat_t2000{ts}};
        
        % comparing baseline2ratings
        actual_recog(ts,:) = ismember(actual_ratings, baseline_emotion) * 100; % -> transform into percentage values -> * 100
    end % trial loop
    
    % get indices for each rated emotional category at first t0-trial
    tmp_idxAf = contains(data_raw.(subjects{iSubj}).cat_t01,'Af');
    tmp_idxAn = contains(data_raw.(subjects{iSubj}).cat_t01,'An');
    tmp_idxHa = contains(data_raw.(subjects{iSubj}).cat_t01,'Ha');
    tmp_idxSa = contains(data_raw.(subjects{iSubj}).cat_t01,'Sa');
    
    % sort+average recognition-accuracies according to time*emotion-combination
    data_recog.t0_Af(iSubj,1) = mean(actual_recog(tmp_idxAf,1));
    data_recog.t0_An(iSubj,1) = mean(actual_recog(tmp_idxAn,1));
    data_recog.t0_Ha(iSubj,1) = mean(actual_recog(tmp_idxHa,1));
    data_recog.t0_Sa(iSubj,1) = mean(actual_recog(tmp_idxSa,1));
    
    data_recog.t500_Af(iSubj,1) = mean(actual_recog(tmp_idxAf,2));
    data_recog.t500_An(iSubj,1) = mean(actual_recog(tmp_idxAn,2));
    data_recog.t500_Ha(iSubj,1) = mean(actual_recog(tmp_idxHa,2));
    data_recog.t500_Sa(iSubj,1) = mean(actual_recog(tmp_idxSa,2));
    
    data_recog.t1000_Af(iSubj,1) = mean(actual_recog(tmp_idxAf,3));
    data_recog.t1000_An(iSubj,1) = mean(actual_recog(tmp_idxAn,3));
    data_recog.t1000_Ha(iSubj,1) = mean(actual_recog(tmp_idxHa,3));
    data_recog.t1000_Sa(iSubj,1) = mean(actual_recog(tmp_idxSa,3));
    
    data_recog.t2000_Af(iSubj,1) = mean(actual_recog(tmp_idxAf,4));
    data_recog.t2000_An(iSubj,1) = mean(actual_recog(tmp_idxAn,4));
    data_recog.t2000_Ha(iSubj,1) = mean(actual_recog(tmp_idxHa,4));
    data_recog.t2000_Sa(iSubj,1) = mean(actual_recog(tmp_idxSa,4));
    
    %% +++++++ d_prime / Criterion +++++++++++++++++++++++++++++++++++++++++++++
    % here, get individual recognition rates for each time_shift, with respect to the first t0-trial
    % loop time_shifts
    for iEMO = 1:4
        % baseline_trials identified as emotion(iEMO)
        idxBaselineEmotion = contains(data_raw.(subjects{iSubj}).cat_t01,orderEmotions{iEMO});
        
        % # of present/absent baseline emotion
        nEmotionPresent = sum(idxBaselineEmotion);
        nEmotionAbsent = sum(~idxBaselineEmotion);
        
        % loop 4 thimeshifts
        for iTS = 1:4
            % get rating
            switch iTS
                case 1
                    idxRatedEmotion = contains(data_raw.(subjects{iSubj}).cat_t02,orderEmotions{iEMO});
                case 2
                    idxRatedEmotion = contains(data_raw.(subjects{iSubj}).cat_t500,orderEmotions{iEMO});
                case 3
                    idxRatedEmotion = contains(data_raw.(subjects{iSubj}).cat_t1000,orderEmotions{iEMO});
                case 4
                    idxRatedEmotion = contains(data_raw.(subjects{iSubj}).cat_t2000,orderEmotions{iEMO});
            end
            
            % count "hits" (rated emotion equal to baseline emotion) and "false alarms" (rated emotion unequal to baseline emotion)
            actualHit = sum(idxRatedEmotion(idxBaselineEmotion == 1));
            actualFA = sum(idxRatedEmotion(idxBaselineEmotion == 0));
            
            % calc "Hit Rate" (HR) and "False Alarm Rate" (FAR)
            actualHR = actualHit./nEmotionPresent;
            actualFAR = actualFA./nEmotionAbsent;
            
            % catch special cases if rates "= 0" or "= 1"
            if actualHR == 1, actualHR = (nEmotionPresent-0.5)./nEmotionPresent; end %#ok<*BDSCI>
            if actualHR == 0, actualHR = 0.5/nEmotionPresent; end
            if actualFAR == 1, actualFAR = (nEmotionAbsent-0.5)./nEmotionAbsent; end
            if actualFAR == 0, actualFAR = 0.5/nEmotionAbsent; end
            
            % calc d' and Criterion
            actual_dPrime.(orderEmotions{iEMO}).(orderTimeShifts{iTS}) = norminv(actualHR)-norminv(actualFAR);
            actual_crit.(orderEmotions{iEMO}).(orderTimeShifts{iTS}) = -1 * (norminv(actualHR) + norminv(actualFAR))/2; 
        end
    end
    
    % sort d' according to time*emotion-combination
    data_dPrime.t0_Af(iSubj,1) = actual_dPrime.Af.t0;
    data_dPrime.t0_An(iSubj,1) = actual_dPrime.An.t0;
    data_dPrime.t0_Ha(iSubj,1) = actual_dPrime.Ha.t0;
    data_dPrime.t0_Sa(iSubj,1) = actual_dPrime.Sa.t0;
    
    data_dPrime.t500_Af(iSubj,1) = actual_dPrime.Af.t500;
    data_dPrime.t500_An(iSubj,1) = actual_dPrime.An.t500;
    data_dPrime.t500_Ha(iSubj,1) = actual_dPrime.Ha.t500;
    data_dPrime.t500_Sa(iSubj,1) = actual_dPrime.Sa.t500;
    
    data_dPrime.t1000_Af(iSubj,1) = actual_dPrime.Af.t1000;
    data_dPrime.t1000_An(iSubj,1) = actual_dPrime.An.t1000;
    data_dPrime.t1000_Ha(iSubj,1) = actual_dPrime.Ha.t1000;
    data_dPrime.t1000_Sa(iSubj,1) = actual_dPrime.Sa.t1000;
    
    data_dPrime.t2000_Af(iSubj,1) = actual_dPrime.Af.t2000;
    data_dPrime.t2000_An(iSubj,1) = actual_dPrime.An.t2000;
    data_dPrime.t2000_Ha(iSubj,1) = actual_dPrime.Ha.t2000;
    data_dPrime.t2000_Sa(iSubj,1) = actual_dPrime.Sa.t2000;
    
    % sort Crit according to time*emotion-combination
    data_crit.t0_Af(iSubj,1) = actual_crit.Af.t0;
    data_crit.t0_An(iSubj,1) = actual_crit.An.t0;
    data_crit.t0_Ha(iSubj,1) = actual_crit.Ha.t0;
    data_crit.t0_Sa(iSubj,1) = actual_crit.Sa.t0;
    
    data_crit.t500_Af(iSubj,1) = actual_crit.Af.t500;
    data_crit.t500_An(iSubj,1) = actual_crit.An.t500;
    data_crit.t500_Ha(iSubj,1) = actual_crit.Ha.t500;
    data_crit.t500_Sa(iSubj,1) = actual_crit.Sa.t500;
    
    data_crit.t1000_Af(iSubj,1) = actual_crit.Af.t1000;
    data_crit.t1000_An(iSubj,1) = actual_crit.An.t1000;
    data_crit.t1000_Ha(iSubj,1) = actual_crit.Ha.t1000;
    data_crit.t1000_Sa(iSubj,1) = actual_crit.Sa.t1000;
    
    data_crit.t2000_Af(iSubj,1) = actual_crit.Af.t2000;
    data_crit.t2000_An(iSubj,1) = actual_crit.An.t2000;
    data_crit.t2000_Ha(iSubj,1) = actual_crit.Ha.t2000;
    data_crit.t2000_Sa(iSubj,1) = actual_crit.Sa.t2000;
    
    % clear variables after this subject
    clear actual_crit actual_dPrime;
end % subject loop

%% ****************** check assumption (normality) ***************************************
disp('     --> check for normality');
% call sub_function to check for normality of data and, if neccessary, apply
% 2-step-transformation. also, return statistics
[data_recog4anova, stats_recog_sw] = sub_shapiro_wilks(data_recog);
[data_dPrime4anova, stats_dPrime_sw] = sub_shapiro_wilks(data_dPrime);
[data_crit4anova, stats_crit_sw] = sub_shapiro_wilks(data_crit);

%% ****************** 2-way repeated measures ANOVA **************************************
disp('     --> perform 2way-repeated-measures-ANOVA');
% Create the within table
fact_timeshift = {'t0','t0','t0','t0','t500','t500','t500','t500','t1000','t1000','t1000','t1000','t2000','t2000','t2000','t2000'}';
fact_emotion = {'Af','An','Ha','Sa','Af','An','Ha','Sa','Af','An','Ha','Sa','Af','An','Ha','Sa'}';
WithinStructure = table(fact_timeshift, fact_emotion, 'VariableNames', {'TimeShift','Emotion'});

% call sub_function and perform
[stats_recog_ranovatable,...
    stats_recog_posthoc_TS,...
    stats_recog_posthoc_EMO,...
    stats_recog_posthoc_TSbyEMO] = sub_2way_rmANOVA(data_recog4anova,WithinStructure);
[stats_dPrime_ranovatable,...
    stats_dPrime_posthoc_TS,...
    stats_dPrime_posthoc_EMO,...
    stats_dPrime_posthoc_TSbyEMO] = sub_2way_rmANOVA(data_dPrime4anova,WithinStructure);
[stats_crit_ranovatable,...
    stats_crit_posthoc_TS,...
    stats_crit_posthoc_EMO,...
    stats_crit_posthoc_TSbyEMO] = sub_2way_rmANOVA(data_crit4anova,WithinStructure);

%% ****************** plotting figures ***************************************************
% style ----------------------------------------------------------------------------------
s.background_color = 'w';

s.faceColor = [.1 .1 .1;...
    .3 .3 .3;...
    .5 .5 .5;...
    .7 .7 .7];

s.violinWidth = .4;
s.alpha = .5;
s.boxColor = [0 0 0];
s.boxWidth = 0.05;
s.medianColor = [1 1 1];

s.sigLineCol = [0 0 0];
s.sigLineWidth = .5;
s.sigAstSize = 14;

s.scatSize = 20;
s.scatColor = [0.2 0.2 0.2];
s.scatAlpha = 0.7;

s.EB_capsize = 0;
s.EB_width = 2;

txt.timeshift = {'+0 ms','+500 ms','+1000 ms','+2000 ms'};
txt.emotions = {'affection','anger','happiness','sadness'};

% ----------------------------------------------------------------------------------------
%% figure *** recognition accuracy -------------------------------------------------------
% ----------------------------------------------------------------------------------------
disp('     --> plotting figure: recognition rates');
fh_acc = figure();
set(fh_acc,'Position',[140 50 1100 440]);
set(fh_acc,'Color',s.background_color);

s.sigLineFirstInt = 3;
s.sigLineInt = 1.5;
s.sigLineStarDist = .075;
s.sigLineStarYOffset = -.7;

% maineffect TIMESHIFT -------------------------------------------------------------------
subplot(1,3,1); cla;
box off; hold on;

% get data
plt_t0 =    mean([data_recog.t0_Af data_recog.t0_An data_recog.t0_Ha data_recog.t0_Sa],2);
plt_t500 =  mean([data_recog.t500_Af data_recog.t500_An data_recog.t500_Ha data_recog.t500_Sa],2);
plt_t1000 = mean([data_recog.t1000_Af data_recog.t1000_An data_recog.t1000_Ha data_recog.t1000_Sa],2);
plt_t2000 = mean([data_recog.t2000_Af data_recog.t2000_An data_recog.t2000_Ha data_recog.t2000_Sa],2);
plt_data = [plt_t0,plt_t500,plt_t1000,plt_t2000];

% violin plot
for ts = 1:4
    vp(ts) = Violin(plt_data(:,ts), ts,...
        'Width',s.violinWidth,...
        'ViolinColor',s.faceColor(ts,:),...
        'ViolinAlpha',s.alpha,...
        'BoxColor',s.boxColor,...
        'BoxWidth',s.boxWidth,...
        'MedianColor',s.medianColor,...
        'ShowMean',true); %#ok<*SAGROW>
    
    % change scatter_point_size
    vp(ts).ScatterPlot.SizeData = s.scatSize;
    vp(ts).ScatterPlot.MarkerFaceColor = s.scatColor;
    vp(ts).ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
end

% significance bars
sigN = sum(stats_recog_posthoc_TS.pValue < 0.05)/2;
sigLineMaxVal = max(plt_data(:));
sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
sigLineI = 1;
for i = 1:3
    for j = 4:-1:i+1
        cmp_ind = contains(stats_recog_posthoc_TS.TimeShift_1,orderTimeShifts{i}) & contains(stats_recog_posthoc_TS.TimeShift_2,orderTimeShifts{j});
        cmp_pVal = stats_recog_posthoc_TS.pValue(cmp_ind);
        
        if cmp_pVal < 0.05
            ast = sub_getStars(cmp_pVal);
            line([i j],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
            text(j + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
            sigLineI = sigLineI + 1;
        end
    end
end

% finetuning
xlim([0.3 4.7]);
ylim([0 max([100, ceil(sigLineHeight)])]);
ax1 = gca;
ax1.XAxis.TickLength = [0 0];
ax1.YGrid = 'on';
set(ax1,...
    'XTick',1:4,...
    'XTickLabelRotation',20,...
    'XTickLabel',txt.timeshift,...
    'FontSize',12)
ylabel('recognition accuracy [%]','FontSize',16);

% interaction TIMESHIFT*EMOTION ----------------------------------------------------------
subplot(1,3,[2 3]); cla;
box off; hold on;

% loop emotions and plot 4-timeshift-violins per emotion
mxLineHeight = 0;
for emo = 1:4
    if emo == 1
        xLoc = 1:4;
        xField = {'t0_Af','t500_Af','t1000_Af','t2000_Af'};
    elseif emo == 2
        xLoc = 6:9;
        xField = {'t0_An','t500_An','t1000_An','t2000_An'};
    elseif emo == 3
        xLoc = 11:14;
        xField = {'t0_Ha','t500_Ha','t1000_Ha','t2000_Ha'};
    elseif emo == 4
        xLoc = 16:19;
        xField = {'t0_Sa','t500_Sa','t1000_Sa','t2000_Sa'};
    end
    
    % draw violin for each timeshift
    mxVal = 0;
    for ts = 1:4
        vp(ts) = Violin(data_recog.(xField{ts}), xLoc(ts),...
            'Width',s.violinWidth,...
            'ViolinColor',s.faceColor(ts,:),...
            'ViolinAlpha',s.alpha,...
            'BoxColor',s.boxColor,...
            'BoxWidth',s.boxWidth,...
            'MedianColor',s.medianColor,...
            'ShowMean',true); %#ok<*SAGROW>
        
        % change scatter_point_size
        vp(ts).ScatterPlot.SizeData = s.scatSize;
        vp(ts).ScatterPlot.MarkerFaceColor = s.scatColor;
        vp(ts).ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
        
        % save max value for sigLines
        mxVal = max([mxVal; data_recog.(xField{ts})]);
    end
    
    % significance bars
    sigEmoInd = strcmp(stats_recog_posthoc_TSbyEMO.Emotion,orderEmotions{emo});
    sigN = sum(stats_recog_posthoc_TSbyEMO.pValue(sigEmoInd) < 0.05)/2;
    sigLineMaxVal = mxVal;
    sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
    sigLineI = 1;
    for i = 1:3
        for j = 4:-1:i+1
            cmp_ind = sigEmoInd & contains(stats_recog_posthoc_TSbyEMO.TimeShift_1,orderTimeShifts{i}) & contains(stats_recog_posthoc_TSbyEMO.TimeShift_2,orderTimeShifts{j});
            cmp_pVal = stats_recog_posthoc_TSbyEMO.pValue(cmp_ind);
            
            if cmp_pVal < 0.05
                ast = sub_getStars(cmp_pVal);
                line([xLoc(i) xLoc(j)],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                text(xLoc(j) + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
                sigLineI = sigLineI + 1;
            end
        end
    end
    if ~isempty(sigLineHeight)
        mxLineHeight = max([mxLineHeight, sigLineHeight(1)]);
    end
end

% finetuning
xlim([0.3 19.7]);
ylim([0 max([100, ceil(mxLineHeight)])]);
ax2 = gca;
ax2.XAxis.TickLength = [0 0];
ax2.YGrid = 'on';
set(ax2,'XTick',[2.5 7.5 12.5 17.5],'XTickLabel',txt.emotions,'FontSize',12);
ylabel('recognition accuracy [%]','FontSize',16);

lgd = [vp(1).ViolinPlot, vp(2).ViolinPlot, vp(3).ViolinPlot, vp(4).ViolinPlot];
lg = legend(lgd,txt.timeshift,...
    'Location','NorthEastOutside',...
    'FontSize',8);
title(lg,legendCaption,'FontSize',10);

% apply max_ylim to both subplots --------------------------------------------------------
ylim(ax1,[0 ceil(max([ax1.YLim ax2.YLim]))]);
ylim(ax2,[0 ceil(max([ax1.YLim ax2.YLim]))]);

% figure-finetuning ----------------------------------------------------------------------
% add A) + B)
annotation('textbox',[.08 .9 .1 .1],'String','A)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.36 .9 .1 .1],'String','B)','FontSize',14,'FontWeight','bold','EdgeColor','none');

% ----------------------------------------------------------------------------------------
%% figure *** dPrime / criterion ---------------------------------------------------------
% ----------------------------------------------------------------------------------------
disp('     --> plotting figure: d''/Crit');
fh_dpC = figure();
set(fh_dpC,'Position',[140 50 1100 440]);
set(fh_dpC,'Color',s.background_color);

% d' settings for significance bars/asterisks
s.sigLineFirstInt_d = 3/15; %.2
s.sigLineInt_d = 3/30; %.08;
s.sigLineStarDist_d = .05;
s.sigLineStarYOffset_d = -0.04;

% crit settings for significance bars/asterisks
s.sigLineFirstInt_c = 1/15;
s.sigLineInt_c = 1/30;
s.sigLineStarDist_c = .05;
s.sigLineStarYOffset_c = -0.011;

%  _dPrime_ maineffect TIMESHIFT ---------------------------------------------------------
subplot(2,4,1); cla;
box off; hold on;

% get data
plt_t0 =    mean([data_dPrime.t0_Af data_dPrime.t0_An data_dPrime.t0_Ha data_dPrime.t0_Sa],2);
plt_t500 =  mean([data_dPrime.t500_Af data_dPrime.t500_An data_dPrime.t500_Ha data_dPrime.t500_Sa],2);
plt_t1000 = mean([data_dPrime.t1000_Af data_dPrime.t1000_An data_dPrime.t1000_Ha data_dPrime.t1000_Sa],2);
plt_t2000 = mean([data_dPrime.t2000_Af data_dPrime.t2000_An data_dPrime.t2000_Ha data_dPrime.t2000_Sa],2);
plt_data = [plt_t0,plt_t500,plt_t1000,plt_t2000];

plt_data_MN = mean(plt_data);
plt_data_SEM = 2 .* std(plt_data) ./ sqrt(nSubj);

% bar plot
cla; bp = sub_barmod(plt_data_MN,1,.2,orderTimeShifts);

bp_x = nan(size(bp))';
for i = 1:size(bp,1)
    bp_x(i) = mean(bp(i).XData);
    bp(i).FaceColor = s.faceColor(i,:) + 0.2; % adjust color, make it a little brighter
    bp(i).EdgeColor = 'none';
end

% error bars
for i = 1:size(bp,1)
    errorbar(bp_x(:,i), plt_data_MN(:,i), plt_data_SEM(:,i),...
        'Color','k',...
        'LineStyle','none',...
        'LineWidth',s.EB_width,...
        'Capsize',s.EB_capsize);
end

% significance bars
sigLineMaxVal = max(plt_data_MN+plt_data_SEM);
sigN = sum(stats_dPrime_posthoc_TS.pValue < 0.05)/2;
sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt_d,sigLineMaxVal+s.sigLineFirstInt_d+(sigN-1)*s.sigLineInt_d,sigN));
sigLineI = 1;
for i = 1:3
    for j = 4:-1:i+1
        cmp_ind = contains(stats_dPrime_posthoc_TS.TimeShift_1,orderTimeShifts{i}) & contains(stats_dPrime_posthoc_TS.TimeShift_2,orderTimeShifts{j});
        cmp_pVal = stats_dPrime_posthoc_TS.pValue(cmp_ind);
        
        if cmp_pVal < 0.05
            ast = sub_getStars(cmp_pVal);
            line([bp_x(i) bp_x(j)],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
            text(bp_x(j)+s.sigLineStarDist_d,sigLineHeight(sigLineI)+s.sigLineStarYOffset_d,ast,'FontSize',s.sigAstSize);
            sigLineI = sigLineI + 1;
        end
    end
end

% finetuning
xlim([bp_x(1)-1 bp_x(end)+1]);
ylim([0 max([0, ceil(sigLineHeight)])]);
ax1 = gca;
ax1.XAxis.TickLength = [0 0];
ax1.YGrid = 'on';
set(ax1,...
    'XTick',[],...
    'FontSize',12)
ylabel('Sensitivity (d'')','FontSize',16);

% _dPrime_ interaction TIMESHIFT * EMOTION ----------------------------------------------
subplot(2,4,[2 3 4]); cla;
box off; hold on;

% get data
plt_af = [data_dPrime.t0_Af data_dPrime.t500_Af data_dPrime.t1000_Af data_dPrime.t2000_Af];
plt_an = [data_dPrime.t0_An data_dPrime.t500_An data_dPrime.t1000_An data_dPrime.t2000_An];
plt_ha = [data_dPrime.t0_Ha data_dPrime.t500_Ha data_dPrime.t1000_Ha data_dPrime.t2000_Ha];
plt_sa = [data_dPrime.t0_Sa data_dPrime.t500_Sa data_dPrime.t1000_Sa data_dPrime.t2000_Sa];

plt_data_MN = [mean(plt_af); mean(plt_an); mean(plt_ha); mean(plt_sa)];
plt_data_SEM = 2 .*  [std(plt_af); std(plt_an); std(plt_ha); std(plt_sa)] ./ sqrt(nSubj);

% bar plot
cla; bp = sub_barmod(plt_data_MN,.2,.2,txt.emotions);

bp_x = nan(size(bp));
for i = 1:size(bp,1)
    for j = 1:size(bp,2)
        bp_x(i,j) = mean(bp(i,j).XData);
        bp(i,j).FaceColor = s.faceColor(j,:) + 0.2; % adjust color, make it a little brighter
        bp(i,j).EdgeColor = 'none';
    end
end

% error bars
for i = 1:size(bp,1)
    errorbar(bp_x(:,i), plt_data_MN(:,i), plt_data_SEM(:,i),...
        'Color','k',...
        'LineStyle','none',...
        'LineWidth',s.EB_width,...
        'Capsize',s.EB_capsize);
end

% loop emotions and plot sign. bars per emotion
mxLineHeight = 0;
for emo = 1:4
    % significance bars
    sigEmoInd = strcmp(stats_dPrime_posthoc_TSbyEMO.Emotion,orderEmotions{emo});
    sigN = sum(stats_dPrime_posthoc_TSbyEMO.pValue(sigEmoInd) < 0.05)/2;
    sigLineMaxVal = max(plt_data_MN(emo,:) + plt_data_SEM(emo,:));
    sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt_d,sigLineMaxVal+s.sigLineFirstInt_d+(sigN-1)*s.sigLineInt_d,sigN));
    sigLineI = 1;
    for i = 1:3
        for j = 4:-1:i+1
            cmp_ind = sigEmoInd & contains(stats_dPrime_posthoc_TSbyEMO.TimeShift_1,orderTimeShifts{i}) & contains(stats_dPrime_posthoc_TSbyEMO.TimeShift_2,orderTimeShifts{j});
            cmp_pVal = stats_dPrime_posthoc_TSbyEMO.pValue(cmp_ind);
            
            if cmp_pVal < 0.05
                ast = sub_getStars(cmp_pVal);
                line([bp_x(emo,i) bp_x(emo,j)],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                text(bp_x(emo,j)+s.sigLineStarDist_d,sigLineHeight(sigLineI)+s.sigLineStarYOffset_d,ast,'FontSize',s.sigAstSize)
                sigLineI = sigLineI + 1;
            end
        end
    end
    if ~isempty(sigLineHeight)
        mxLineHeight = max([mxLineHeight, sigLineHeight(1)]);
    end
end

% finetuning
xlim([0.5 4.7]);
ylim([0 max([0, ceil(mxLineHeight)])]);
ax2 = gca;
ax2.XAxis.TickLength = [0 0];
ax2.YGrid = 'on';
set(ax2,'FontSize',12,'XTickLabel',{});

% legend
lg = legend(bp(1,:),txt.timeshift,...
    'Location','NorthEastOutside',...
    'FontSize',8);
title(lg,legendCaption,'FontSize',10);

% apply max_ylim to both subplots --------------------------------------------------------
ylim(ax1,[0 ceil(max([ax1.YLim ax2.YLim]))]);
ylim(ax2,[0 ceil(max([ax1.YLim ax2.YLim]))]);

%  _Crit_ maineffect TIMESHIFT -----------------------------------------------------------
subplot(2,4,5); cla;
box off; hold on;

% get data
plt_t0 =    mean([data_crit.t0_Af data_crit.t0_An data_crit.t0_Ha data_crit.t0_Sa],2);
plt_t500 =  mean([data_crit.t500_Af data_crit.t500_An data_crit.t500_Ha data_crit.t500_Sa],2);
plt_t1000 = mean([data_crit.t1000_Af data_crit.t1000_An data_crit.t1000_Ha data_crit.t1000_Sa],2);
plt_t2000 = mean([data_crit.t2000_Af data_crit.t2000_An data_crit.t2000_Ha data_crit.t2000_Sa],2);
plt_data = [plt_t0,plt_t500,plt_t1000,plt_t2000];

plt_data_MN = mean(plt_data);
plt_data_SEM = 2 .* std(plt_data) ./ sqrt(nSubj);

% bar plot
cla; bp = sub_barmod(plt_data_MN,1,.2,orderTimeShifts);

bp_x = nan(size(bp))';
for i = 1:size(bp,1)
    bp_x(i) = mean(bp(i).XData);
    bp(i).FaceColor = s.faceColor(i,:) + 0.2; % adjust color, make it a little brighter
    bp(i).EdgeColor = 'none';
end

% error bars
for i = 1:size(bp,1)
    errorbar(bp_x(:,i), plt_data_MN(:,i), plt_data_SEM(:,i),...
        'Color','k',...
        'LineStyle','none',...
        'LineWidth',s.EB_width,...
        'Capsize',s.EB_capsize);
end

% significance bars
sigLineMaxVal = max(plt_data_MN+plt_data_SEM);
sigN = sum(stats_crit_posthoc_TS.pValue < 0.05)/2;
sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt_c,sigLineMaxVal+s.sigLineFirstInt_c+(sigN-1)*s.sigLineInt_c,sigN));
sigLineI = 1;
for i = 1:3
    for j = 4:-1:i+1
        cmp_ind = contains(stats_crit_posthoc_TS.TimeShift_1,orderTimeShifts{i}) & contains(stats_crit_posthoc_TS.TimeShift_2,orderTimeShifts{j});
        cmp_pVal = stats_crit_posthoc_TS.pValue(cmp_ind);
        
        if cmp_pVal < 0.05
            ast = sub_getStars(cmp_pVal);
            line([bp_x(i) bp_x(j)],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
            text(bp_x(j)+s.sigLineStarDist_c,sigLineHeight(sigLineI)+s.sigLineStarYOffset_c,ast,'FontSize',s.sigAstSize)
            sigLineI = sigLineI + 1;
        end
    end
end

% finetuning
xlim([bp_x(1)-1 bp_x(end)+1]);
ylim([0 max([0, ceil(sigLineHeight)])]);
ax1 = gca;
ax1.XAxis.TickLength = [0 0];
ax1.YGrid = 'on';
set(ax1,...
    'XTick',bp_x,...
    'XTickLabelRotation',20,...
    'XTickLabel',txt.timeshift,...
    'FontSize',12)
ylabel('Criterion (c)','FontSize',16);

% _Crit_ interaction TIMESHIFT * EMOTION ------------------------------------------------
subplot(2,4,[6 7 8]); cla;
box off; hold on;

% get data
plt_af = [data_crit.t0_Af data_crit.t500_Af data_crit.t1000_Af data_crit.t2000_Af];
plt_an = [data_crit.t0_An data_crit.t500_An data_crit.t1000_An data_crit.t2000_An];
plt_ha = [data_crit.t0_Ha data_crit.t500_Ha data_crit.t1000_Ha data_crit.t2000_Ha];
plt_sa = [data_crit.t0_Sa data_crit.t500_Sa data_crit.t1000_Sa data_crit.t2000_Sa];

plt_data_MN = [mean(plt_af); mean(plt_an); mean(plt_ha); mean(plt_sa)];
plt_data_SEM = 2 .*  [std(plt_af); std(plt_an); std(plt_ha); std(plt_sa)] ./ sqrt(nSubj);

% bar plot
cla; bp = sub_barmod(plt_data_MN,.2,.2,txt.emotions);

bp_x = nan(size(bp));
for i = 1:size(bp,1)
    for j = 1:size(bp,2)
        bp_x(i,j) = mean(bp(i,j).XData);
        bp(i,j).FaceColor = s.faceColor(j,:) + 0.2; % adjust color, make it a little brighter
        bp(i,j).EdgeColor = 'none';
    end
end

% error bars
for i = 1:size(bp,1)
    errorbar(bp_x(:,i), plt_data_MN(:,i), plt_data_SEM(:,i),...
        'Color','k',...
        'LineStyle','none',...
        'LineWidth',s.EB_width,...
        'Capsize',s.EB_capsize);
end

% loop emotions and plot sign. bars per emotion
mxLineHeight = 0;
for emo = 1:4
    % significance bars
    sigEmoInd = strcmp(stats_crit_posthoc_TSbyEMO.Emotion,orderEmotions{emo});
    sigN = sum(stats_crit_posthoc_TSbyEMO.pValue(sigEmoInd) < 0.05)/2;
    sigLineMaxVal = max(plt_data_MN(emo,:) + plt_data_SEM(emo,:));
    sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt_c,sigLineMaxVal+s.sigLineFirstInt_c+(sigN-1)*s.sigLineInt_c,sigN));
    sigLineI = 1;
    for i = 1:3
        for j = 4:-1:i+1
            cmp_ind = sigEmoInd & contains(stats_crit_posthoc_TSbyEMO.TimeShift_1,orderTimeShifts{i}) & contains(stats_crit_posthoc_TSbyEMO.TimeShift_2,orderTimeShifts{j});
            cmp_pVal = stats_crit_posthoc_TSbyEMO.pValue(cmp_ind);
            
            if cmp_pVal < 0.05
                ast = sub_getStars(cmp_pVal);
                line([bp_x(emo,i) bp_x(emo,j)],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                text(bp_x(emo,j)+s.sigLineStarDist_c,sigLineHeight(sigLineI)+s.sigLineStarYOffset_c,ast,'FontSize',s.sigAstSize)
                sigLineI = sigLineI + 1;
            end
        end
    end
    if ~isempty(sigLineHeight)
        mxLineHeight = max([mxLineHeight, sigLineHeight(1)]);
    end
end

% finetuning
xlim([0.5 4.7]);
ylim([0 max([0, ceil(mxLineHeight)])]);
ax2 = gca;
ax2.XAxis.TickLength = [0 0];
ax2.YGrid = 'on';
set(ax2,'FontSize',12);

% legend
lg = legend(bp(1,:),txt.timeshift,...
    'Location','NorthEastOutside',...
    'FontSize',8);
title(lg,legendCaption,'FontSize',10);

% apply max_ylim to both subplots --------------------------------------------------------
ylim(ax1,[0 ceil(max([ax1.YLim ax2.YLim]))]);
ylim(ax2,[0 ceil(max([ax1.YLim ax2.YLim]))]);

% figure-finetuning ----------------------------------------------------------------------
% add A) B) C) D)
annotation('textbox',[.08 .9 .1 .1],'String','A)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.3 .9 .1 .1],'String','B)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.08 .42 .1 .1],'String','C)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.3 .42 .1 .1],'String','D)','FontSize',14,'FontWeight','bold','EdgeColor','none');

%% ****************** save data and figures **********************************************
if saving_data == true
    disp('     --> saving analysis results');
    % convert class of specific table_coloumns (don't know why, but neccessary)
    stats_recog_ranovatable.F = double(stats_recog_ranovatable.F);
    stats_recog_ranovatable.pValue = double(stats_recog_ranovatable.pValue);
    stats_recog_ranovatable.pValueGG = double(stats_recog_ranovatable.pValueGG);
    stats_recog_ranovatable.pValueHF = double(stats_recog_ranovatable.pValueHF);
    stats_recog_ranovatable.pValueLB = double(stats_recog_ranovatable.pValueLB);

    stats_dPrime_ranovatable.F = double(stats_dPrime_ranovatable.F);
    stats_dPrime_ranovatable.pValue = double(stats_dPrime_ranovatable.pValue);
    stats_dPrime_ranovatable.pValueGG = double(stats_dPrime_ranovatable.pValueGG);
    stats_dPrime_ranovatable.pValueHF = double(stats_dPrime_ranovatable.pValueHF);
    stats_dPrime_ranovatable.pValueLB = double(stats_dPrime_ranovatable.pValueLB);

    stats_crit_ranovatable.F = double(stats_crit_ranovatable.F);
    stats_crit_ranovatable.pValue = double(stats_crit_ranovatable.pValue);
    stats_crit_ranovatable.pValueGG = double(stats_crit_ranovatable.pValueGG);
    stats_crit_ranovatable.pValueHF = double(stats_crit_ranovatable.pValueHF);
    stats_crit_ranovatable.pValueLB = double(stats_crit_ranovatable.pValueLB);
    
    % data_tables
    stats_recog_sw.Properties.DimensionNames{1} = 'condition';
    stats_dPrime_sw.Properties.DimensionNames{1} = 'condition';
    stats_crit_sw.Properties.DimensionNames{1} = 'condition';
    
    writetable(struct2table(data_recog),fullfile(folder.data,'AnalysisB_recognition_subject_data.csv'));
    writetable(stats_recog_ranovatable,fullfile(folder.data,'AnalysisB_recognition_stats_anova.csv'),'WriteRowNames',true);
    writetable(stats_recog_posthoc_TS,fullfile(folder.data,'AnalysisB_recognition_stats_postHoc_timeshift.csv'));
    writetable(stats_recog_posthoc_EMO,fullfile(folder.data,'AnalysisB_recognition_stats_postHoc_emotion.csv'));
    writetable(stats_recog_posthoc_TSbyEMO,fullfile(folder.data,'AnalysisB_recognition_stats_postHoc_timeshiftBYemotion.csv'));
    writetable(stats_recog_sw,fullfile(folder.data,'AnalysisB_recognition_Shapiro_Wilks.csv'),'WriteRowNames',true);
    
    writetable(struct2table(data_dPrime),fullfile(folder.data,'AnalysisB_dPrime_subject_data.csv'));
    writetable(stats_dPrime_ranovatable,fullfile(folder.data,'AnalysisB_dPrime_stats_anova.csv'),'WriteRowNames',true);
    writetable(stats_dPrime_posthoc_TS,fullfile(folder.data,'AnalysisB_dPrime_stats_postHoc_timeshift.csv'));
    writetable(stats_dPrime_posthoc_EMO,fullfile(folder.data,'AnalysisB_dPrime_stats_postHoc_emotion.csv'));
    writetable(stats_dPrime_posthoc_TSbyEMO,fullfile(folder.data,'AnalysisB_dPrime_stats_postHoc_timeshiftBYemotion.csv'));
    writetable(stats_dPrime_sw,fullfile(folder.data,'AnalysisB_dPrime_Shapiro_Wilks.csv'),'WriteRowNames',true); 
    
    writetable(struct2table(data_crit),fullfile(folder.data,'AnalysisB_Criterion_subject_data.csv'));
    writetable(stats_crit_ranovatable,fullfile(folder.data,'AnalysisB_Criterion_stats_anova.csv'),'WriteRowNames',true);
    writetable(stats_crit_posthoc_TS,fullfile(folder.data,'AnalysisB_Criterion_stats_postHoc_timeshift.csv'));
    writetable(stats_crit_posthoc_EMO,fullfile(folder.data,'AnalysisB_Criterion_stats_postHoc_emotion.csv'));
    writetable(stats_crit_posthoc_TSbyEMO,fullfile(folder.data,'AnalysisB_Criterion_stats_postHoc_timeshiftBYemotion.csv'));
    writetable(stats_crit_sw,fullfile(folder.data,'AnalysisB_Criterion_Shapiro_Wilks.csv'),'WriteRowNames',true); 
end

if saving_figs == true
    disp('     --> saving figures');

    savefig(fh_acc,fullfile(folder.figs,'AnalysisB_fig3_recognition_accuracy'));
    exportgraphics(fh_acc, fullfile(folder.figs,'AnalysisB_fig3_recognition_accuracy.pdf'),'ContentType','vector');

    savefig(fh_dpC,fullfile(folder.figs,'AnalysisB_fig4_conditions_dPrime_Criterion'));
    exportgraphics(fh_dpC, fullfile(folder.figs,'AnalysisB_fig4_conditions_dPrime_Criterion.pdf'),'ContentType','vector');
end

%% ****************** displaying anova results *******************************
disp('     --> displaying statistical results of rmANOVA');
% recognition accuracies -----------------------------------------------------------------
switch stats_recog_ranovatable.correction{3}
    case 'none'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_recog_ranovatable.mauchlyDF(3),...
                                        stats_recog_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_recog_ranovatable.mauchlyPvalue(3)));
        
        this_df1_TS = sprintf('%d',stats_recog_ranovatable.DF(3));
        this_df2_TS = sprintf('%d',stats_recog_ranovatable.DF(4));
        this_F_TS = stats_recog_ranovatable.F(3);
        this_p_TS = stats_recog_ranovatable.pValue(3);
        this_pETAsq_TS = stats_recog_ranovatable.pEtaSq(3);
        
    case 'GG'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_recog_ranovatable.mauchlyDF(3),...
                                        stats_recog_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_recog_ranovatable.mauchlyPvalue(3)),...
                                        stats_recog_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_recog_ranovatable.dfGG(3));
        this_df2_TS = sprintf('%.2f',stats_recog_ranovatable.dfGG(4));
        this_F_TS = stats_recog_ranovatable.F(3);
        this_p_TS = stats_recog_ranovatable.pValueGG(3);
        this_pETAsq_TS = stats_recog_ranovatable.pEtaSq(3);
        
    case 'HF'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_recog_ranovatable.mauchlyDF(3),...
                                        stats_recog_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_recog_ranovatable.mauchlyPvalue(3)),...
                                        stats_recog_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_recog_ranovatable.dfHF(3));
        this_df2_TS = sprintf('%.2f',stats_recog_ranovatable.dfHF(4));
        this_F_TS = stats_recog_ranovatable.F(3);
        this_p_TS = stats_recog_ranovatable.pValueHF(3);
        this_pETAsq_TS = stats_recog_ranovatable.pEtaSq(3);
end

switch stats_recog_ranovatable.correction{5}
    case 'none'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_recog_ranovatable.mauchlyDF(5),...
                                        stats_recog_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_recog_ranovatable.mauchlyPvalue(5)));
        
        this_df1_EMO = sprintf('%d',stats_recog_ranovatable.DF(5));
        this_df2_EMO = sprintf('%d',stats_recog_ranovatable.DF(6));
        this_F_EMO = stats_recog_ranovatable.F(5);
        this_p_EMO = stats_recog_ranovatable.pValue(5);
        this_pETAsq_EMO = stats_recog_ranovatable.pEtaSq(5);
        
    case 'GG'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_recog_ranovatable.mauchlyDF(5),...
                                        stats_recog_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_recog_ranovatable.mauchlyPvalue(5)),...
                                        stats_recog_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_recog_ranovatable.dfGG(5));
        this_df2_EMO = sprintf('%.2f',stats_recog_ranovatable.dfGG(6));
        this_F_EMO = stats_recog_ranovatable.F(5);
        this_p_EMO = stats_recog_ranovatable.pValueGG(5);
        this_pETAsq_EMO = stats_recog_ranovatable.pEtaSq(5);
        
    case 'HF'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_recog_ranovatable.mauchlyDF(5),...
                                        stats_recog_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_recog_ranovatable.mauchlyPvalue(5)),...
                                        stats_recog_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_recog_ranovatable.dfHF(5));
        this_df2_EMO = sprintf('%.2f',stats_recog_ranovatable.dfHF(6));
        this_F_EMO = stats_recog_ranovatable.F(5);
        this_p_EMO = stats_recog_ranovatable.pValueHF(5);
        this_pETAsq_EMO = stats_recog_ranovatable.pEtaSq(5);
end

switch stats_recog_ranovatable.correction{7}
    case 'none'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                            stats_recog_ranovatable.mauchlyDF(7),...
                                            stats_recog_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_recog_ranovatable.mauchlyPvalue(7)));
        
        this_df1_TSbyEMO = sprintf('%d',stats_recog_ranovatable.DF(7));
        this_df2_TSbyEMO = sprintf('%d',stats_recog_ranovatable.DF(8));
        this_F_TSbyEMO = stats_recog_ranovatable.F(7);
        this_p_TSbyEMO = stats_recog_ranovatable.pValue(7);
        this_pETAsq_TSbyEMO = stats_recog_ranovatable.pEtaSq(7);
        
    case 'GG'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                            stats_recog_ranovatable.mauchlyDF(7),...
                                            stats_recog_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_recog_ranovatable.mauchlyPvalue(7)),...
                                            stats_recog_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_recog_ranovatable.dfGG(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_recog_ranovatable.dfGG(8));
        this_F_TSbyEMO = stats_recog_ranovatable.F(7);
        this_p_TSbyEMO = stats_recog_ranovatable.pValueGG(7);
        this_pETAsq_TSbyEMO = stats_recog_ranovatable.pEtaSq(7);
        
    case 'HF'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                            stats_recog_ranovatable.mauchlyDF(7),...
                                            stats_recog_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_recog_ranovatable.mauchlyPvalue(7)),...
                                            stats_recog_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_recog_ranovatable.dfHF(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_recog_ranovatable.dfHF(8));
        this_F_TSbyEMO = stats_recog_ranovatable.F(7);
        this_p_TSbyEMO = stats_recog_ranovatable.pValueHF(7);
        this_pETAsq_TSbyEMO = stats_recog_ranovatable.pEtaSq(7);
end

disp(' ');
disp('         --- ANOVA results: recognition accuracy ---------------------------------------');
fprintf('             main effect -> TimeShift\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TS,this_df1_TS,this_df2_TS,this_F_TS,sub_getPString(this_p_TS),this_pETAsq_TS)
fprintf('             main effect -> Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_EMO,this_df1_EMO,this_df2_EMO,this_F_EMO,sub_getPString(this_p_EMO),this_pETAsq_EMO)
fprintf('             interaction -> TimeShift * Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TSbyEMO,this_df1_TSbyEMO,this_df2_TSbyEMO,this_F_TSbyEMO,sub_getPString(this_p_TSbyEMO),this_pETAsq_TSbyEMO)

% dPrime ---------------------------------------------------------------------------------
switch stats_dPrime_ranovatable.correction{3}
    case 'none'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_dPrime_ranovatable.mauchlyDF(3),...
                                        stats_dPrime_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(3)));
        
        this_df1_TS = sprintf('%d',stats_dPrime_ranovatable.DF(3));
        this_df2_TS = sprintf('%d',stats_dPrime_ranovatable.DF(4));
        this_F_TS = stats_dPrime_ranovatable.F(3);
        this_p_TS = stats_dPrime_ranovatable.pValue(3);
        this_pETAsq_TS = stats_dPrime_ranovatable.pEtaSq(3);
        
    case 'GG'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_dPrime_ranovatable.mauchlyDF(3),...
                                        stats_dPrime_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(3)),...
                                        stats_dPrime_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_dPrime_ranovatable.dfGG(3));
        this_df2_TS = sprintf('%.2f',stats_dPrime_ranovatable.dfGG(4));
        this_F_TS = stats_dPrime_ranovatable.F(3);
        this_p_TS = stats_dPrime_ranovatable.pValueGG(3);
        this_pETAsq_TS = stats_dPrime_ranovatable.pEtaSq(3);
        
    case 'HF'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_dPrime_ranovatable.mauchlyDF(3),...
                                        stats_dPrime_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(3)),...
                                        stats_dPrime_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_dPrime_ranovatable.dfHF(3));
        this_df2_TS = sprintf('%.2f',stats_dPrime_ranovatable.dfHF(4));
        this_F_TS = stats_dPrime_ranovatable.F(3);
        this_p_TS = stats_dPrime_ranovatable.pValueHF(3);
        this_pETAsq_TS = stats_dPrime_ranovatable.pEtaSq(3);
end

switch stats_dPrime_ranovatable.correction{5}
    case 'none'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_dPrime_ranovatable.mauchlyDF(5),...
                                        stats_dPrime_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(5)));
        
        this_df1_EMO = sprintf('%d',stats_dPrime_ranovatable.DF(5));
        this_df2_EMO = sprintf('%d',stats_dPrime_ranovatable.DF(6));
        this_F_EMO = stats_dPrime_ranovatable.F(5);
        this_p_EMO = stats_dPrime_ranovatable.pValue(5);
        this_pETAsq_EMO = stats_dPrime_ranovatable.pEtaSq(5);
        
    case 'GG'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_dPrime_ranovatable.mauchlyDF(5),...
                                        stats_dPrime_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(5)),...
                                        stats_dPrime_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_dPrime_ranovatable.dfGG(5));
        this_df2_EMO = sprintf('%.2f',stats_dPrime_ranovatable.dfGG(6));
        this_F_EMO = stats_dPrime_ranovatable.F(5);
        this_p_EMO = stats_dPrime_ranovatable.pValueGG(5);
        this_pETAsq_EMO = stats_dPrime_ranovatable.pEtaSq(5);
        
    case 'HF'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_dPrime_ranovatable.mauchlyDF(5),...
                                        stats_dPrime_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(5)),...
                                        stats_dPrime_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_dPrime_ranovatable.dfHF(5));
        this_df2_EMO = sprintf('%.2f',stats_dPrime_ranovatable.dfHF(6));
        this_F_EMO = stats_dPrime_ranovatable.F(5);
        this_p_EMO = stats_dPrime_ranovatable.pValueHF(5);
        this_pETAsq_EMO = stats_dPrime_ranovatable.pEtaSq(5);
end

switch stats_dPrime_ranovatable.correction{7}
    case 'none'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                            stats_dPrime_ranovatable.mauchlyDF(7),...
                                            stats_dPrime_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(7)));
        
        this_df1_TSbyEMO = sprintf('%d',stats_dPrime_ranovatable.DF(7));
        this_df2_TSbyEMO = sprintf('%d',stats_dPrime_ranovatable.DF(8));
        this_F_TSbyEMO = stats_dPrime_ranovatable.F(7);
        this_p_TSbyEMO = stats_dPrime_ranovatable.pValue(7);
        this_pETAsq_TSbyEMO = stats_dPrime_ranovatable.pEtaSq(7);
        
    case 'GG'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                            stats_dPrime_ranovatable.mauchlyDF(7),...
                                            stats_dPrime_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(7)),...
                                            stats_dPrime_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_dPrime_ranovatable.dfGG(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_dPrime_ranovatable.dfGG(8));
        this_F_TSbyEMO = stats_dPrime_ranovatable.F(7);
        this_p_TSbyEMO = stats_dPrime_ranovatable.pValueGG(7);
        this_pETAsq_TSbyEMO = stats_dPrime_ranovatable.pEtaSq(7);
        
    case 'HF'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                            stats_dPrime_ranovatable.mauchlyDF(7),...
                                            stats_dPrime_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_dPrime_ranovatable.mauchlyPvalue(7)),...
                                            stats_dPrime_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_dPrime_ranovatable.dfHF(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_dPrime_ranovatable.dfHF(8));
        this_F_TSbyEMO = stats_dPrime_ranovatable.F(7);
        this_p_TSbyEMO = stats_dPrime_ranovatable.pValueHF(7);
        this_pETAsq_TSbyEMO = stats_dPrime_ranovatable.pEtaSq(7);
end

disp(' ');
disp('           --- ANOVA results: d'' --------------------------------------------------------');
fprintf('             main effect -> TimeShift\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TS,this_df1_TS,this_df2_TS,this_F_TS,sub_getPString(this_p_TS),this_pETAsq_TS)
fprintf('             main effect -> Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_EMO,this_df1_EMO,this_df2_EMO,this_F_EMO,sub_getPString(this_p_EMO),this_pETAsq_EMO)
fprintf('             interaction -> TimeShift * Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TSbyEMO,this_df1_TSbyEMO,this_df2_TSbyEMO,this_F_TSbyEMO,sub_getPString(this_p_TSbyEMO),this_pETAsq_TSbyEMO)

% Criterion ---------------------------------------------------------------------------------
switch stats_crit_ranovatable.correction{3}
    case 'none'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_crit_ranovatable.mauchlyDF(3),...
                                        stats_crit_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_crit_ranovatable.mauchlyPvalue(3)));
        
        this_df1_TS = sprintf('%d',stats_crit_ranovatable.DF(3));
        this_df2_TS = sprintf('%d',stats_crit_ranovatable.DF(4));
        this_F_TS = stats_crit_ranovatable.F(3);
        this_p_TS = stats_crit_ranovatable.pValue(3);
        this_pETAsq_TS = stats_crit_ranovatable.pEtaSq(3);
        
    case 'GG'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_crit_ranovatable.mauchlyDF(3),...
                                        stats_crit_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_crit_ranovatable.mauchlyPvalue(3)),...
                                        stats_crit_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_crit_ranovatable.dfGG(3));
        this_df2_TS = sprintf('%.2f',stats_crit_ranovatable.dfGG(4));
        this_F_TS = stats_crit_ranovatable.F(3);
        this_p_TS = stats_crit_ranovatable.pValueGG(3);
        this_pETAsq_TS = stats_crit_ranovatable.pEtaSq(3);
        
    case 'HF'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_crit_ranovatable.mauchlyDF(3),...
                                        stats_crit_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_crit_ranovatable.mauchlyPvalue(3)),...
                                        stats_crit_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_crit_ranovatable.dfHF(3));
        this_df2_TS = sprintf('%.2f',stats_crit_ranovatable.dfHF(4));
        this_F_TS = stats_crit_ranovatable.F(3);
        this_p_TS = stats_crit_ranovatable.pValueHF(3);
        this_pETAsq_TS = stats_crit_ranovatable.pEtaSq(3);
end

switch stats_crit_ranovatable.correction{5}
    case 'none'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_crit_ranovatable.mauchlyDF(5),...
                                        stats_crit_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_crit_ranovatable.mauchlyPvalue(5)));
        
        this_df1_EMO = sprintf('%d',stats_crit_ranovatable.DF(5));
        this_df2_EMO = sprintf('%d',stats_crit_ranovatable.DF(6));
        this_F_EMO = stats_crit_ranovatable.F(5);
        this_p_EMO = stats_crit_ranovatable.pValue(5);
        this_pETAsq_EMO = stats_crit_ranovatable.pEtaSq(5);
        
    case 'GG'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_crit_ranovatable.mauchlyDF(5),...
                                        stats_crit_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_crit_ranovatable.mauchlyPvalue(5)),...
                                        stats_crit_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_crit_ranovatable.dfGG(5));
        this_df2_EMO = sprintf('%.2f',stats_crit_ranovatable.dfGG(6));
        this_F_EMO = stats_crit_ranovatable.F(5);
        this_p_EMO = stats_crit_ranovatable.pValueGG(5);
        this_pETAsq_EMO = stats_crit_ranovatable.pEtaSq(5);
        
    case 'HF'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_crit_ranovatable.mauchlyDF(5),...
                                        stats_crit_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_crit_ranovatable.mauchlyPvalue(5)),...
                                        stats_crit_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_crit_ranovatable.dfHF(5));
        this_df2_EMO = sprintf('%.2f',stats_crit_ranovatable.dfHF(6));
        this_F_EMO = stats_crit_ranovatable.F(5);
        this_p_EMO = stats_crit_ranovatable.pValueHF(5);
        this_pETAsq_EMO = stats_crit_ranovatable.pEtaSq(5);
end

switch stats_crit_ranovatable.correction{7}
    case 'none'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                            stats_crit_ranovatable.mauchlyDF(7),...
                                            stats_crit_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_crit_ranovatable.mauchlyPvalue(7)));
        
        this_df1_TSbyEMO = sprintf('%d',stats_crit_ranovatable.DF(7));
        this_df2_TSbyEMO = sprintf('%d',stats_crit_ranovatable.DF(8));
        this_F_TSbyEMO = stats_crit_ranovatable.F(7);
        this_p_TSbyEMO = stats_crit_ranovatable.pValue(7);
        this_pETAsq_TSbyEMO = stats_crit_ranovatable.pEtaSq(7);
        
    case 'GG'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                            stats_crit_ranovatable.mauchlyDF(7),...
                                            stats_crit_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_crit_ranovatable.mauchlyPvalue(7)),...
                                            stats_crit_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_crit_ranovatable.dfGG(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_crit_ranovatable.dfGG(8));
        this_F_TSbyEMO = stats_crit_ranovatable.F(7);
        this_p_TSbyEMO = stats_crit_ranovatable.pValueGG(7);
        this_pETAsq_TSbyEMO = stats_crit_ranovatable.pEtaSq(7);
        
    case 'HF'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                            stats_crit_ranovatable.mauchlyDF(7),...
                                            stats_crit_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_crit_ranovatable.mauchlyPvalue(7)),...
                                            stats_crit_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_crit_ranovatable.dfHF(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_crit_ranovatable.dfHF(8));
        this_F_TSbyEMO = stats_crit_ranovatable.F(7);
        this_p_TSbyEMO = stats_crit_ranovatable.pValueHF(7);
        this_pETAsq_TSbyEMO = stats_crit_ranovatable.pEtaSq(7);
end

disp(' ');
disp('           --- ANOVA results: Criterion --------------------------------------------------------');
fprintf('             main effect -> TimeShift\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TS,this_df1_TS,this_df2_TS,this_F_TS,sub_getPString(this_p_TS),this_pETAsq_TS)
fprintf('             main effect -> Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_EMO,this_df1_EMO,this_df2_EMO,this_F_EMO,sub_getPString(this_p_EMO),this_pETAsq_EMO)
fprintf('             interaction -> TimeShift * Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TSbyEMO,this_df1_TSbyEMO,this_df2_TSbyEMO,this_F_TSbyEMO,sub_getPString(this_p_TSbyEMO),this_pETAsq_TSbyEMO)

%% finished
disp(' ');
disp('   >>> done <<<');
