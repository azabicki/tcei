function results = sub_wilcoxon(iA,iB)
%% perform Wilcoxon test
[WX.p,~,WX.stats] = signrank(iA,iB);

%% calculate effect size as matched rank-biserial correlation
dDiff = iA-iB;

epsdiff = eps(iA) + eps(iB);
t = (abs(dDiff) <= epsdiff);
dDiff(t) = [];
epsdiff(t) = [];

tr = tiedrank(abs(dDiff),0,0,epsdiff);
W(1) = sum(tr(dDiff>0));
W(2) = sum(tr(dDiff<0));

n = size(dDiff,1);
WX.r = 4 * (min(W) - mean(W)) / (n*(n+1));

% correct sign of r
WX.r = WX.r .* sign(diff(W));

%% sorting/saving results into output variable
results.pValue = WX.p;
results.r = WX.r;
results.Z = WX.stats.zval;
results.signedRank = WX.stats.signedrank;
