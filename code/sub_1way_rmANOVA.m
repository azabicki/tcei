function [ranovatable, posthoc] = sub_1way_rmANOVA(data,WithinStructure)

% prepare data
factors = WithinStructure.Properties.VariableNames;

fns = fieldnames(data);
conditions = '';
for i = 1:numel(fns)
    conditions = [conditions fns{i} ',']; %#ok<AGROW>
end
conditions = [conditions(1:end-1) '~1'];

% fit the repeated measures model
rmModel = fitrm(struct2table(data),conditions,'WithinDesign',WithinStructure);

% get results for rmANOVA + post-hoc pairwise comparisons
[ranovatable,~,cMat] = ranova(rmModel);
posthoc = multcompare(rmModel,factors{1},'ComparisonType','bonferroni');

% calculate partial eta^2
ranovatable.pEtaSq(1) = ranovatable.SumSq(1)/sum(ranovatable.SumSq(1:2));

% calculat mauchly test + epsilon and add to anova table
res_mauchly = mauchly(rmModel,cMat);
res_epsilon = epsilon(rmModel,cMat);

ranovatable.mauchlyChiSq = [res_mauchly.ChiStat'; 0];
ranovatable.mauchlyDF = [res_mauchly.DF';0];
ranovatable.mauchlyPvalue = [res_mauchly.pValue';0];
ranovatable.epsilonGG = repmat(res_epsilon.GreenhouseGeisser',[2 1]);
ranovatable.epsilonHF = repmat(res_epsilon.HuynhFeldt',[2 1]);

% adjust degrees of freedom
ranovatable.dfGG = ranovatable.DF .* ranovatable.epsilonGG;
ranovatable.dfHF = ranovatable.DF .* ranovatable.epsilonHF;

% decide if, and which, correction will be used, according to Girden (1992)
if ranovatable.mauchlyPvalue(1) > 0.05
    ranovatable.correction{1} = 'none';
elseif ranovatable.epsilonGG(1) > .75
    ranovatable.correction{1} = 'HF';
else
    ranovatable.correction{1} = 'GG';
end
