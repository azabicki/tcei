function cols = sub_createColormap(positions, colors)
% FUNCTION "createColormap" defines a customized colobar given the positions and the 
% colors that are going to generate the gradients.

nCols = 256;

% Compute positions along the samples
colSamp = round((nCols-1)*positions)+1;

% Make the gradients among colors
cols = zeros(nCols,3);
cols(colSamp,:) = colors;
diffS = diff(colSamp)-1;
for d = 1:1:length(diffS)
    if diffS(d)~=0
        col1 = colors(d,:);
        col2 = colors(d+1,:);
        G = zeros(diffS(d),3);
        for idx = 1:3
            g = linspace(col1(idx), col2(idx), diffS(d)+2);
            g([1, length(g)]) = [];
            G(:,idx) = g';
        end
        cols(colSamp(d)+1:colSamp(d+1)-1,:) = G;
    end
end
end


