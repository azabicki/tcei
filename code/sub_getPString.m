function pStr = sub_getPString(p)
if p < 0.001
    pStr = 'p < .001';
elseif p < 0.01
    pStr = 'p < .01';
elseif p < 0.05
    pStr = 'p < .05';
else
    pStr = sprintf('p = %0.2f', p);
end
end

