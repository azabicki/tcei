function hBar = sub_barmod(x,bo,bgo,labels)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reading in "x":
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Storing "x" size data
szx = size(x);
NBG = szx(1); %number of bar groups (data points/set)
n = szx(2); %number of bars/group (data sets)

%In case of row vector input, make column vector
if NBG == 1
    x = x';
    szx = size(x);
    NBG = szx(1);
    n = szx(2);
end

%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%                           Plotting bar chart
%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
hBar = bar(x,'hist');
hold on

%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%               Important bar chart modification parameters
%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%Bar group width (bgw) system defaults (determined experimentally)
if n == 1
    bgw = 1;
elseif n <= 6
    bgw = 2*n/(2*n + 3);
else
    bgw = .8;
end
bw = bgw/n; %bar width
bgs = 1 - bgw; %bar group space (width of space between bar groups)

%Original x-coordinate of each bar centroid
oc = x; %(same size as input "x")
for i = 1:NBG
    for j = 1:n
        oc(i,j) = i - bgw*(n-1)/(2*n) + (j-1)*bgw/n; %original centers
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reading in "bo"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bo = bo*bw; %scaling to bar width

%creating a total bar group width that accounts for bar offest
tbgw = bgw + bo*(n-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reading in "bgo"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bgo = bgo*tbgw; %scaling to total bar group width

%Displacement of each bar
d = x; %distance each bar moves (same size as x)
for i = 1:NBG
    for j= 1:n
        d(i,j) = i*(bgs-bgo-(n-1)*bo) - j*bo;
    end
end
nbc = oc-d; %new bar center
nbgc = mean(nbc,2); %new bar group center
    
%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%                        Applying bar group labels
%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reading in "labels"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isempty(labels)
    for i = 1:NBG
        labels{i} = strcat('title',num2str(i));
    end
end

%centering x-ticks to each bar group
ax = gca;
xticks(nbgc);

%applying labels to x-ticks
ax.XAxis.TickLabels = labels;

%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%                        Adjusting patch vertices
%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%

%Storing patch .Vertices arrays
v = cell(1);
for i = 1:n
    v{i} = hBar(i).Vertices;
end


%Creating index array for .Vertices arrays
szv = length(v{1}); %length of .Vertices arrays
ia = zeros(1,szv); %indexing array declaration
for i = 1:szv
    if floor((i-1)/5)==(i-1)/5
        ia(i) = 1; %indexing array
    else
        ia(i) = 0;
    end
end


%Adjusting vertices with displacement variable "d"
v2 = cell(NBG,n);
for j = 1:n
    for k = 1:NBG
        for i = 1:szv
            if ia(i) ~= 1 && i > 5*k-4 && i <= 5*k
                v{j}(i,1) =  v{j}(i,1) - d(k,j);
                hBar(j).Vertices = v{j};
            end
        end
        v2{k,j} = v{j}(5*k-3:5*k,:);
    end
end

%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%                  Individualizing bars in patch vertices
%~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~%
%clear hBar data, but leave plot axes
delete(hBar)
%Setting parula (default color map) matrix
clrs = parula(n);
%Making individual patches for each bar
for j = 1:n
    for k = 1:NBG
        hBar(k,j) = patch(v2{k,j}(:,1),v2{k,j}(:,2),clrs(j,:));
        hold on
    end
end
end