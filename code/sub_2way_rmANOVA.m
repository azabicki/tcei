function [ranovatable, posthoc_F1, posthoc_F2, posthoc_F1byF2] = sub_2way_rmANOVA(data,WithinStructure)

% prepare data
factors = WithinStructure.Properties.VariableNames;

fns = fieldnames(data);
conditions = '';
for i = 1:numel(fns)
    conditions = [conditions fns{i} ',']; %#ok<AGROW>
end
conditions = [conditions(1:end-1) '~1'];

% fit the repeated measures model
rmModel = fitrm(struct2table(data),conditions,'WithinDesign',WithinStructure);

% get results for rmANOVA + post-hoc pairwise comparisons
[ranovatable,~,cMat] = ranova(rmModel, 'WithinModel',[factors{1} '*' factors{2}]);
posthoc_F1 = multcompare(rmModel,factors{1},'ComparisonType','bonferroni');
posthoc_F2 = multcompare(rmModel,factors{2},'ComparisonType','bonferroni');
posthoc_F1byF2 = multcompare(rmModel,factors{1},'By',factors{2},'ComparisonType','bonferroni');

% calculate partial eta^2
ranovatable.pEtaSq(3) = ranovatable.SumSq(3)/sum(ranovatable.SumSq(3:4));
ranovatable.pEtaSq(5) = ranovatable.SumSq(5)/sum(ranovatable.SumSq(5:6));
ranovatable.pEtaSq(7) = ranovatable.SumSq(7)/sum(ranovatable.SumSq(7:8));

% calculat mauchly test + epsilon and add to anova table
res_mauchly = mauchly(rmModel,cMat);
res_epsilon = epsilon(rmModel,cMat);

ranovatable.mauchlyChiSq = reshape([res_mauchly.ChiStat';zeros(1,4)],numel(res_mauchly.ChiStat)*2,1);
ranovatable.mauchlyDF = reshape([res_mauchly.DF';zeros(1,4)],numel(res_mauchly.DF)*2,1);
ranovatable.mauchlyPvalue = reshape([res_mauchly.pValue';zeros(1,4)],numel(res_mauchly.pValue)*2,1);
ranovatable.epsilonGG = reshape(repmat(res_epsilon.GreenhouseGeisser',[2 1]),numel(res_epsilon.GreenhouseGeisser)*2,1);
ranovatable.epsilonHF = reshape(repmat(res_epsilon.HuynhFeldt',[2 1]),numel(res_epsilon.HuynhFeldt)*2,1);

% adjust degrees of freedom 
ranovatable.dfGG = ranovatable.DF .* ranovatable.epsilonGG;
ranovatable.dfHF = ranovatable.DF .* ranovatable.epsilonHF;

% decide if, and which, correction will be used, according to Girden (1992)
for i = 3:2:7
    if ranovatable.mauchlyPvalue(i) > 0.05
        ranovatable.correction{i} = 'none';
    elseif ranovatable.epsilonGG(i) > .75
        ranovatable.correction{i} = 'HF';
    else
        ranovatable.correction{i} = 'GG';
    end
end