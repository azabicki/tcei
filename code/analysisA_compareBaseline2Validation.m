%% Analysis A: comparing emotion category ratings of baseline sequences (first + 0ms trials) 
%              with external validated emotional categories
% 
% what happens here:
%   - create confusion-matrix
%   - test recognition rates of each emotion against chance 
%   - investigate influence of BDI2/STAI-scores on recognition rate via spearman correlation analysis
% 
% code that leads to 
%   - figure 2 in the manuscript
%   - figure A.1 in Appendix A
%   - statistical values (see output in command window)
%   
% used References:
% Tomczak, M., & Tomczak, E. (2014). The need to report effect size estimates revisited. 
%       An overview of some recommended measures of effect size. Trends Sport Sci. 1(21), 19�25.

clear all; close all;
disp(' *** Analysis A: comparing emotion category ratings of baseline sequences (first + 0ms trials) with external validated emotional categories');

%% options *******************************************************************************
saving_data = true;
saving_figs = true;

% folder
folder.data = fullfile('..','analysis_results');
folder.figs = fullfile('..','analysis_figures');

%% init vars *****************************************************************************
disp('     --> load raw experimental data');
[data_raw, data_subj] = sub_load_txt_files(folder); % load experimental data
data_scores = readtable(fullfile(folder.data,'subjects_information_scores.csv'));

nSubj = numel(data_subj);
nStim = 68;
nEmo = 4;

matColMap = sub_createColormap([0 .01 .1 1],[1 1 1;.95 .95 .95;.8 .8 .8;0 .27 .53]);    % colormap 

% order of emotional categories
emoOrder = {'Af','An','Ha','Sa'};
emotions = {'affection','anger','happiness','sadness'};

%% prepare data for confusion matrix and tests *******************************************
disp('     --> preparing data');
confMatrix = zeros(4,4);
recog_abs = zeros(nSubj,4);

for iSubj = 1:nSubj
    for iStim = 1:nStim
        % find indices for validated and rated emotion category
        tmp_idxV = find(ismember(emoOrder, data_raw.(data_subj{iSubj}).stimCat{iStim}));
        tmp_idxR = find(ismember(emoOrder, data_raw.(data_subj{iSubj}).cat_t01{iStim}));
        
        % store hit/noHit for each subject and each stimulus, spereated by validated emotion
        recog_abs(iSubj,tmp_idxV) = recog_abs(iSubj,tmp_idxV) + double(tmp_idxV == tmp_idxR);
        
        % update confusion matrix
        confMatrix(tmp_idxV,tmp_idxR) = confMatrix(tmp_idxV,tmp_idxR) + 1;
    end
end

% calculate recognition rate for each subject
recog_rates = recog_abs ./ (nStim/4) .* 100;
recog_rates_MN = mean(recog_rates);
recog_rates_SEM = std(recog_rates) ./ sqrt(nSubj);

% number of ttrials per validated emotion
nPerEmotion = sum(confMatrix(1,:));

% add totals
confMatrix_total = [confMatrix,sum(confMatrix,2);[sum(confMatrix,1),nan]];

% percentual confusion matrix
confMatrix_perc = confMatrix/nPerEmotion*100;
confMatrix_total_perc = confMatrix_total/nPerEmotion*100;

%% statistical testing of recognition rates against chance/each other ********************
disp('     --> statistical analysis');

% +++++++++ prepare table
stats_recogVSchance = array2table([recog_rates_MN',recog_rates_SEM'],...
        'VariableNames',{'recognition_rate_MEAN','recognition_rate_SEM'},...
        'RowNames',emoOrder);
stats_recogVSchance.Properties.DimensionNames{1} = 'Emotion';
    
% +++++++++ check assumption (normality) via Shapiro-Wilks 
[~, stats_recog_sw] = sub_shapiro_wilks(recog_rates,emoOrder);

% +++++++++ do statistics, depending on normality of data
if any(stats_recog_sw.p_Value < 0.05)
    disp('        --> please note: here we use original data and fed them into non-parametric tests, thus the templeton-Transformation is obsolete');
    
    % test against chance level via wilcoxon signed rank test
    for i = 1:4
        [tmp_P,~,tmp_Stat] = signrank(recog_rates(:,i),25);
        
        % calculate effect size as pearson correlation coefficient
        tmp_R = abs(tmp_Stat.zval / sqrt(numel(recog_rates(:,i))));
        
        % edit stat_table
        stats_recogVSchance.Wilcoxon_Z(emoOrder{i}) = tmp_Stat.zval;
        stats_recogVSchance.Wilcoxon_pValue(emoOrder{i}) = tmp_P;
        stats_recogVSchance.effect_size_R(emoOrder{i}) = tmp_R;
    end
else
    disp('       --> normality given, using parametric tests. *** BUT NOT IMPLEMENTED DUE TO GIVEN DATA ***');
end

%% investigating influence of questionnaire scores 
% prepare table
stats_BDI = cell2table(emotions','VariableNames',{'emotions'});
stats_STAI = cell2table(emotions','VariableNames',{'emotions'});

% loop emotions
for i = 1:4
    % calculate correlations
    [tmp_R_BDI,tmp_P_BDI] = corr(recog_rates(:,i),data_scores.BDI_II,'Type','Spearman');
    [tmp_R_X1,tmp_P_X1] = corr(recog_rates(:,i),data_scores.STAI_X1,'Type','Spearman');
    [tmp_R_X2,tmp_P_X2] = corr(recog_rates(:,i),data_scores.STAI_X2,'Type','Spearman');
    
    % update table
    stats_BDI.r(i) = tmp_R_BDI;
    stats_BDI.p(i) = tmp_P_BDI;
    stats_STAI.X1_r(i) = tmp_R_X1;
    stats_STAI.X1_p(i) = tmp_P_X1;
    stats_STAI.X2_r(i) = tmp_R_X2;
    stats_STAI.X2_p(i) = tmp_P_X2;
end

%% plotting confusion marix **************************************************************
disp('     --> plotting figure 1: recognition rates compared to validation');
fh = figure('Position',[100 100 1500 380],'Color',[1 1 1]);

% style ----------------------------------------------------------------------------------
s.faceColor = [.5 .5 .5];

s.EB_capsize = 0;
s.EB_width = 2;

s.chanceLineCol = [.1 .1 .1];
s.chanceLineWidth = 2;
s.chanceLineStyle = '--';

s.sigAstInt = 3;
s.sigAstSize = 22;

% matrix ---------------------------------------------------------------------------------
subplot(1,3,1); cla; ax1 = gca;
hold on; axis square; box on;

imagesc(confMatrix_perc,[0 100])

xlim([0.5 4.5]); ylim([0.5 4.5]);
set(gca,'TickLength',[1 1],...
    'YTickLabelRotation',90,...
    'Ydir','reverse',...
    'xaxisLocation','top',...
    'XTick',1:4,...
    'XTicklabel',emotions,...
    'YTick',1:4,...
    'YTicklabel',emotions,...
    'FontSize',10);
xlabel('rated emotion of baseline trials','FontSize',16);
ylabel('validated emotion','FontSize',16);

line([0.5 4.5],[0.5 0.5],'Color','k');    line([0.5 4.5],[4.5 4.5],'Color','k');
line([0.5 0.5],[0.5 4.5],'Color','k');    line([4.5 4.5],[0.5 4.5],'Color','k');

colormap(matColMap)
c = colorbar;
c.Label.String = '[%]';
c.Label.FontSize = 12;

% table ----------------------------------------------------------------------------------
subplot(1,3,2); cla; ax2 = gca;
hold on; axis equal; box off;
xlim([0.5 5.5]);
ylim([0.5 5.5]);
set(gca,'TickLength',[0 0],'YTickLabelRotation',90,'Ydir','reverse','xaxisLocation','top',...
    'XTick',1:5,'XTicklabel',[emotions,{'sum'}],...
    'YTick',1:5,'YTicklabel',[emotions,{'sum'}],...
    'FontSize',9);
xlabel('rated emotion of baseline trials','FontSize',16);
ylabel('validated emotion','FontSize',16);

line([0.5 5.5],[0.5 0.5],'Color','k');    line([0.5 5.5],[1.5 1.5],'Color','k');    line([0.5 5.5],[2.5 2.5],'Color','k');    line([0.5 5.5],[3.5 3.5],'Color','k');    line([0.5 5.5],[4.5 4.5],'Color','k','LineWidth',3);    line([0.5 5.5],[5.5 5.5],'Color','k');
line([0.5 0.5],[0.5 5.5],'Color','k');    line([1.5 1.5],[0.5 5.5],'Color','k');    line([2.5 2.5],[0.5 5.5],'Color','k');    line([3.5 3.5],[0.5 5.5],'Color','k');    line([4.5 4.5],[0.5 5.5],'Color','k','LineWidth',3);    line([5.5 5.5],[0.5 5.5],'Color','k');
line([0.5 1.5],[0.5 0.5],'Color','k','LineWidth',1.5); line([0.5 2.5],[1.5 1.5],'Color','k','LineWidth',1.5); line([1.5 3.5],[2.5 2.5],'Color','k','LineWidth',1.5); line([2.5 4.5],[3.5 3.5],'Color','k','LineWidth',1.5);
line([0.5 0.5],[0.5 1.5],'Color','k','LineWidth',1.5); line([1.5 1.5],[0.5 2.5],'Color','k','LineWidth',1.5); line([2.5 2.5],[1.5 3.5],'Color','k','LineWidth',1.5); line([3.5 3.5],[2.5 4.5],'Color','k','LineWidth',1.5);

for i = 1:5
    for j = 1:5
        if ~isnan(confMatrix_total_perc(i,j))
            if i == j
                txtB = 'bold';
            else
                txtB = 'normal';
            end
            str1 = ['\fontsize{14}' num2str(confMatrix_total(i,j))];
            str2 = ['\fontsize{10}' sprintf(' %.2f%%',confMatrix_total_perc(i,j))];
            text(j,i,{str1, str2},'HorizontalAlignment','center','FontWeight',txtB);
        end
    end
end

% barplot --------------------------------------------------------------------------------
subplot(1,3,3); cla; ax3 = gca;
hold on; axis square; box off;

% bar plot
bp = sub_barmod(recog_rates_MN,.2,.2,emotions);

bp_x = nan(size(bp));
for i = 1:nEmo
    bp_x(i) = mean(bp(i).XData);
    bp(i).FaceColor = s.faceColor;
    bp(i).EdgeColor = 'none';
end

% error bars
for i = 1:nEmo
    errorbar(bp_x(i), recog_rates_MN(i), recog_rates_SEM(i),...
        'Color','k',...
        'LineStyle','none',...
        'LineWidth',s.EB_width,...
        'Capsize',s.EB_capsize);
end

% asterisks showing significant differences to chance
for i = 1:nEmo
	ast = sub_getStars(stats_recogVSchance.Wilcoxon_pValue(i));
	text(bp_x(i),stats_recogVSchance.recognition_rate_MEAN(i)+stats_recogVSchance.recognition_rate_SEM(i)+s.sigAstInt,...
        ast,...
        'FontSize',s.sigAstSize,...
        'HorizontalAlignment','center')
end

% finetuning
xlim([bp_x(1)-.7 bp_x(end)+.7]);
ylim([0 100]);
ax3.XAxis.TickLength = [0 0];
ax3.YGrid = 'on';
line(xlim,[25 25],'Color',s.chanceLineCol,'LineWidth',s.chanceLineWidth,'LineStyle',s.chanceLineStyle);
set(gca,'FontSize',12)
ylabel('recognition rate [%]','FontSize',16);

% figure-finetuning ----------------------------------------------------------------------
% add A) + B)
annotation('textbox',[.08 .8 .1 .1],'String','A)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.67 .8 .1 .1],'String','B)','FontSize',14,'FontWeight','bold','EdgeColor','none');

% move axes
set(ax1,'Position',[.1300 .06 .1743 .6681]);
set(ax2,'Position',[.4108 .06 .1743 .6681]);
set(ax3,'Position',[.6916 .11 .2134 .6681]);

%% plotting scores information **************************************************************
disp('     --> plotting figure 2: influence of individual scores');
fh2 = figure('Position',[100 100 900 700],'Color',[1 1 1]);

% loop emotions
for i = 1:4
    % BDI_II -> scatter plot
    subplot(3,4,i); hold on; box on; axis square;
    scatter(data_scores.BDI_II,recog_rates(:,i),30,'filled');
    lsl = lsline; lsl.LineWidth = 2;
    xlabel('BDI-II score');
    ylabel('recognition rate [%]');
    xlim([0 50]);
    t = title(['\fontsize{10}spearman r(' num2str(nSubj-2) ') = ' sprintf('%.2f',stats_BDI.r(i)) ', ' sub_getPString(stats_BDI.p(i))]);
    set(t,'Position',get(t,'Position').*[1 1.02 1]);
    set(gca,'XTick',0:10:50);
    ylim([0 100]);
    
    % STAI X1 -> scatter plot
    subplot(3,4,i+4); hold on; box on; axis square;
    scatter(data_scores.STAI_X1,recog_rates(:,i),30,'filled');
    lsl = lsline; lsl.LineWidth = 2;
    xlabel('STAI X1 score');
    ylabel('recognition rate [%]');
    t = title(['\fontsize{10}spearman r(' num2str(nSubj-2) ') = ' sprintf('%.2f',stats_STAI.X1_r(i)) ', ' sub_getPString(stats_STAI.X1_p(i))]);
    set(t,'Position',get(t,'Position').*[1 1.02 1]);
    set(gca,'XTick',20:10:60);
    ylim([0 100]);
    
    % STAI X2 -> scatter plot
    subplot(3,4,i+8); hold on; box on; axis square;
    scatter(data_scores.STAI_X2,recog_rates(:,i),30,'filled');
    lsl = lsline; lsl.LineWidth = 2;
    ylabel('recognition rate [%]');
    xlabel('STAI X2 score');
    t = title(['\fontsize{10}spearman r(' num2str(nSubj-2) ') = ' sprintf('%.2f',stats_STAI.X2_r(i)) ', ' sub_getPString(stats_STAI.X2_p(i))]);
    set(t,'Position',get(t,'Position').*[1 1.02 1]);
    set(gca,'XTick',20:20:80);
    ylim([0 100]);
end

% figure-finetuning ----------------------------------------------------------------------
% add A) + B)
annotation('textbox',[.02 .9 .1 .1],'String','A)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.02 .58 .1 .1],'String','B)','FontSize',14,'FontWeight','bold','EdgeColor','none');

% column_labels
annotation('textbox',[.1475 .94 .139 .05],'String','Affection','FontSize',16,'FontWeight','bold','EdgeColor','none','HorizontalAlignment','center');
annotation('textbox',[.3588 .94 .134 .05],'String','Anger','FontSize',16,'FontWeight','bold','EdgeColor','none','HorizontalAlignment','center');
annotation('textbox',[.5643 .94 .134 .05],'String','Happiness','FontSize',16,'FontWeight','bold','EdgeColor','none','HorizontalAlignment','center');
annotation('textbox',[.7719 .94 .132 .05],'String','Sadness','FontSize',16,'FontWeight','bold','EdgeColor','none','HorizontalAlignment','center');

% row_labels
annotation('textarrow',[0 0],[0 0],'string','BDI - II', ...
    'HeadStyle','none','LineStyle', 'none','TextRotation',90,...
    'FontSize',16,'FontWeight','bold',...
    'Position',[.06 .86 0 0]);
annotation('textarrow',[0 0],[0 0],'string','STAI - X1', ...
    'HeadStyle','none','LineStyle', 'none','TextRotation',90,...
    'FontSize',16,'FontWeight','bold',...
    'Position',[.06 .58 0 0]);
annotation('textarrow',[0 0],[0 0],'string','STAI - X2', ...
    'HeadStyle','none','LineStyle', 'none','TextRotation',90,...
    'FontSize',16,'FontWeight','bold',...
    'Position',[.06 .28 0 0]);

%% save data and figures *****************************************************************
if saving_data == true
    disp('     --> saving analysis results');
    
    % confusion matrix data_tables
    T_CM_perc = array2table(confMatrix_total_perc,...
        'VariableNames',[emoOrder,{'sum'}],...
        'RowNames',[emoOrder,{'sum'}]);
    T_CM_perc.Properties.DimensionNames{1} = 'emotion';
    
    T_CM_total = array2table(confMatrix_total,...
        'VariableNames',[emoOrder,{'sum'}],...
        'RowNames',[emoOrder,{'sum'}]);
    T_CM_total.Properties.DimensionNames{1} = 'emotion';
    
    writetable(T_CM_total,fullfile(folder.data,'AnalysisA_confMatrix_total.csv'),'WriteRowNames',true);
    writetable(T_CM_perc,fullfile(folder.data,'AnalysisA_confMatrix_perc.csv'),'WriteRowNames',true);
    
    % statistical results
    recog_rates = array2table(recog_rates,...
        'VariableNames',emoOrder);
        
    stats_recog_sw.Test_Statistic_transformed = [];
    stats_recog_sw.p_Value_transformed = [];
    stats_recog_sw.Properties.DimensionNames{1} = 'Emotion';

    writetable(recog_rates,fullfile(folder.data,'AnalysisA_recognition_subject_data.csv'));
    writetable(stats_recogVSchance,fullfile(folder.data,'AnalysisA_recognition_stats_vs_chance.csv'),'WriteRowNames',true);    
    writetable(stats_recog_sw,fullfile(folder.data,'AnalysisA_recognition_Shapiro_Wilks.csv'),'WriteRowNames',true);
    
    % influence of scores
    writetable(stats_BDI,fullfile(folder.data,'AnalysisA_influence_of_BDI_scores.csv'));
    writetable(stats_STAI,fullfile(folder.data,'AnalysisA_influence_of_STAI_scores.csv'));
end

if saving_figs == true
    disp('     --> saving figures');
    
    savefig(fh,fullfile(folder.figs,'AnalysisA_fig2_confMatrix'));
    exportgraphics(fh,fullfile(folder.figs,'AnalysisA_fig2_confMatrix.pdf'),'ContentType','vector');
    
    savefig(fh2,fullfile(folder.figs,'AnalysisA_figS1_influenceOfScores'));
    exportgraphics(fh2,fullfile(folder.figs,'AnalysisA_figS1_influenceOfScores.pdf'),'ContentType','vector');
end

%% displaying anova results *******************************
disp('     --> displaying statistical results of rmANOVA');
disp(' ');
disp('         --- per Emotion: recognition rates + test vs chance via Wilcoxon one sample signed rank test ------------------------------');
fprintf('             Affection -> \n\t\t\t\trecognition rate: %.3f %% +- %.3f %%\n\t\t\t\tabove chance: Z = %.3f, %s, R = %.3f\n',...
    stats_recogVSchance.recognition_rate_MEAN('Af'),stats_recogVSchance.recognition_rate_SEM('Af'),stats_recogVSchance.Wilcoxon_Z('Af'),sub_getPString(stats_recogVSchance.Wilcoxon_pValue('Af')),stats_recogVSchance.effect_size_R('Af'));
fprintf('             Anger -> \n\t\t\t\trecognition rate: %.3f %% +- %.3f %%\n\t\t\t\tabove chance: Z = %.3f, %s, R = %.3f\n',...
    stats_recogVSchance.recognition_rate_MEAN('An'),stats_recogVSchance.recognition_rate_SEM('An'),stats_recogVSchance.Wilcoxon_Z('An'),sub_getPString(stats_recogVSchance.Wilcoxon_pValue('An')),stats_recogVSchance.effect_size_R('An'));
fprintf('             Happiness -> \n\t\t\t\trecognition rate: %.3f %% +- %.3f %%\n\t\t\t\tabove chance: Z = %.3f, %s, R = %.3f\n',...
    stats_recogVSchance.recognition_rate_MEAN('Ha'),stats_recogVSchance.recognition_rate_SEM('Ha'),stats_recogVSchance.Wilcoxon_Z('Ha'),sub_getPString(stats_recogVSchance.Wilcoxon_pValue('Ha')),stats_recogVSchance.effect_size_R('Ha'));
fprintf('             Sadness -> \n\t\t\t\trecognition rate: %.3f %% +- %.3f %%\n\t\t\t\tabove chance: Z = %.3f, %s, R = %.3f\n',...
    stats_recogVSchance.recognition_rate_MEAN('Sa'),stats_recogVSchance.recognition_rate_SEM('Sa'),stats_recogVSchance.Wilcoxon_Z('Sa'),sub_getPString(stats_recogVSchance.Wilcoxon_pValue('Sa')),stats_recogVSchance.effect_size_R('Sa'));

%% finished
disp(' ');
disp('   >>> done <<<');
