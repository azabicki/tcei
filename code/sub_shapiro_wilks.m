function [data4anova, stats] = sub_shapiro_wilks(input,input_fns)

% prepare data depending on input_data_class
if isstruct(input)
    fns = fieldnames(input);
    data = nan(size(input.(fns{1}),1),numel(fns));
    for i = 1:numel(fns)
        data(:,i) = input.(fns{i});
    end
else
    fns = input_fns(:);
    data = input;
end

% init vars
nVars = size(data,2);
sw_pre = nan(nVars,2);
sw_post = nan(nVars,2);

% shapiro wilks test
for i = 1:nVars
    [sw_pre(i,1),sw_pre(i,2)] = sub_swtest(data(:,i));
end

% init stats_table
stats = array2table(sw_pre,...
    'VariableNames',{'Test_Statistic','p_Value'},...
    'RowNames',fns);

% transform recognition_accuracy_data
if any(sw_pre(:,2) < 0.05)
    disp('        --> normality not given one of the conditions. using 2-step-transformation (see Templeton, 2011).');
    data_transformed = nan(size(data));
    for i = 1:numel(fns)
        d = data(:,i);
        % 2-step-transformation
        fr = tiedrank(d) ./ numel(d);
        fr(fr==1) = 1-(1/numel(d));
        data_transformed(:,i) = norminv(fr,mean(d),std(d));
        % shapiro-wilks again
        [sw_post(i,1),sw_post(i,2)] = sub_swtest(data_transformed(:,i));
    end
    data = data_transformed;
    stats.('Test_Statistic_transformed') = sw_post(:,1);
    stats.('p_Value_transformed') = sw_post(:,2);
end

% prepare output_data depending on input_data_class
if isstruct(input)
    for i = 1:numel(fns)
        data4anova.(fns{i}) = data(:,i);
    end
else
    data4anova = data;
end

end

%% SW-Test
function [sw,p,h] = sub_swtest(x)
% if n*1, then 1*n
x = x(:)';

% Alpha value can be changed as required
alpha = 0.05;

% SHAPIRO-WILK TEST
n = length(x);
y=sort(x);
a = [];
i = 1:n;
mi = norminv((i-0.375)/(n+0.25));
u = 1/sqrt(n);
m = mi.^2;

a(n) = -2.706056*(u^5)+4.434685*(u^4)-2.07119*(u^3)-0.147981*(u^2)+0.221157*u+mi(n)/sqrt(sum(m));
a(n-1) = -3.58263*(u^5)+5.682633*(u^4)-1.752461*(u^3)-0.293762*(u^2)+0.042981*u+mi(n-1)/sqrt(sum(m));
a(1) = -a(n);
a(2) = -a(n-1);
eps = (sum(m)-2*(mi(n)^2)-2*(mi(n-1)^2))/(1-2*(a(n)^2)-2*(a(n-1)^2));
a(3:n-2) = mi(3:n-2)./sqrt(eps);
ax = a.*y;
KT = sum((x-mean(x)).^2);
b = sum(ax)^2;
SWtest = b/KT;
mu = 0.0038915*(log(n)^3)-0.083751*(log(n)^2)-0.31082*log(n)-1.5861;
sigma = exp(0.0030302*(log(n)^2)-0.082676*log(n)-0.4803);
z = (log(1-SWtest)-mu)/sigma;
pvalue = 1-normcdf(z,0,1);

% results
sw = SWtest;
p = pvalue;

% Compare p-value to alpha
if p > alpha
    h = 1;
else
    h = 0;
end
end