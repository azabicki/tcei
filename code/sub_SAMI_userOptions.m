function userOptions = sub_SAMI_userOptions(folder)
%% **********************************************************
% Project details
% **********************************************************
% This name identifies a collection of files which all belong to the same run of a project.
userOptions.analysisName = 'TCEI_SAMI_results';

% This is the root directory of the project.
userOptions.rootPath = fullfile(pwd,folder.sami_root);

% The path leading to where the c3d files are stored.
userOptions.c3dPath = fullfile(pwd,folder.c3d);

%% **********************************************************
% c3d file and label settings
% **********************************************************
% if markers are available, but named differently, use this to rename labels of "ownMarker" 
% into "samiMarker" [Head, LSHO, LELB, LWRI, LHIP, LKNE, LANK, RSHO, RELB, RWRI, RHIP, RKNE, RANK]
userOptions.c3d_OwnMarker = true;
userOptions.c3d_MarkerMatching = {...
    'ownMarker','samiMarker';...
    'HAND','WRI';...
    };

%% **********************************************************
% descriptions of stimuli: regarding their categories, and how to sort them
% **********************************************************
% set category which is used to sort the stimuli according to
userOptions.stimuli_sorting = 'emotion';

% if filename is specified: "stimulus_settings" will be loaded from this file
userOptions.stimuli_settings_filename = 'sami_stimuli_settings.txt';
% else: stimulus_settings have to be defined here
userOptions.stimuli_settings = {};
end
