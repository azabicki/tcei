function [data_raw, data_subj] = sub_load_txt_files(folder)
%% load 

% folder containing experimental data
folder.exp_data = fullfile('..','raw_exp_data');

% fetch content of folder 
files_raw = dir(fullfile(folder.exp_data,'*.txt'));

% init vars
nSubj = numel(files_raw);
data_subj = cell(nSubj,1);

% loop files
for i = 1:nSubj
    data_raw.(files_raw(i).name(1:6)) = readtable(fullfile(files_raw(i).folder,files_raw(i).name));
    data_subj{i} = files_raw(i).name(1:6);
end

end