%% Analysis C: investigating influence of temporal offset on valence perception by means of 
%              comparing absolute valence ratings (=intensity) of temporally decoupled sequences
% 
% code that leads to 
%   - figure 5 in the manuscript
%   - figure A.2 in the  Appendix A, showing distribution of actual valence ratings with respect to validated 
%       emotion categories (not based on baseline-emotion as in fig 5)
%   - statistical values (see output in command window)
% 
% used References:
% Girden, E. R. (1992). ANOVA: Repeated measures. Sage university papers. Quantitative 
%       applications in the social sciences: no. 07-084. Newbury Park, Calif.: Sage Publications.
% Templeton, G.F. (2011). A Two-Step Approach for Transforming Continuous Variables to 
%       Normal: Implications and Recommendations for IS Research. Communications of the AIS, Vol. 28, Article 4.
% Tomczak, M., & Tomczak, E. (2014). The need to report effect size estimates revisited. 
%       An overview of some recommended measures of effect size. Trends Sport Sci. 1(21), 19�25.

clear all; close all;
disp(' *** Analysis C: investigating influence of temporal offset on valence perception');

%% options *******************************************************************************
saving_data = true;
saving_figs = true;

% folder
folder.data = fullfile('..','analysis_results');
folder.figs = fullfile('..','analysis_figures');

%% init vars *****************************************************************************
disp('     --> load raw experimental data');
[data_raw, subjects] = sub_load_txt_files(folder); % load experimental data

nSubj = numel(subjects);
nStim = 68;

% order of emotional categories and timeshifts
orderEmotions = {'Af','An','Ha','Sa'};
orderTimeShifts = {'t0','t500','t1000','t2000'};
orderTimeShifts_bl = {'bl','t0','t500','t1000','t2000'};
legendCaption = 'temporal offset';

%% ****************** fetch relevant data ************************************************
disp('     --> calculate absolute valence ratings');
% loop subjects and fetch relevant data
for iSubj = 1:nSubj
	% rated valence of shifted trials
    actual_valence = [data_raw.(subjects{iSubj}).val_t02,...
                      data_raw.(subjects{iSubj}).val_t500,...
                      data_raw.(subjects{iSubj}).val_t1000,...
                      data_raw.(subjects{iSubj}).val_t2000];
    
    % get indices for each rated emotional category at first t0-trial
    tmp_idxAf = contains(data_raw.(subjects{iSubj}).cat_t01,'Af');
    tmp_idxAn = contains(data_raw.(subjects{iSubj}).cat_t01,'An');
    tmp_idxHa = contains(data_raw.(subjects{iSubj}).cat_t01,'Ha');
    tmp_idxSa = contains(data_raw.(subjects{iSubj}).cat_t01,'Sa');
    
    % sort+average absoulute valence-ratings (=intensity) according to time*emotion-combination
    data_val_int.t0_Af(iSubj,1) = mean(abs(actual_valence(tmp_idxAf,1)));
    data_val_int.t0_An(iSubj,1) = mean(abs(actual_valence(tmp_idxAn,1)));
    data_val_int.t0_Ha(iSubj,1) = mean(abs(actual_valence(tmp_idxHa,1)));
    data_val_int.t0_Sa(iSubj,1) = mean(abs(actual_valence(tmp_idxSa,1)));
    
    data_val_int.t500_Af(iSubj,1) = mean(abs(actual_valence(tmp_idxAf,2)));
    data_val_int.t500_An(iSubj,1) = mean(abs(actual_valence(tmp_idxAn,2)));
    data_val_int.t500_Ha(iSubj,1) = mean(abs(actual_valence(tmp_idxHa,2)));
    data_val_int.t500_Sa(iSubj,1) = mean(abs(actual_valence(tmp_idxSa,2)));
    
    data_val_int.t1000_Af(iSubj,1) = mean(abs(actual_valence(tmp_idxAf,3)));
    data_val_int.t1000_An(iSubj,1) = mean(abs(actual_valence(tmp_idxAn,3)));
    data_val_int.t1000_Ha(iSubj,1) = mean(abs(actual_valence(tmp_idxHa,3)));
    data_val_int.t1000_Sa(iSubj,1) = mean(abs(actual_valence(tmp_idxSa,3)));
    
    data_val_int.t2000_Af(iSubj,1) = mean(abs(actual_valence(tmp_idxAf,4)));
    data_val_int.t2000_An(iSubj,1) = mean(abs(actual_valence(tmp_idxAn,4)));
    data_val_int.t2000_Ha(iSubj,1) = mean(abs(actual_valence(tmp_idxHa,4)));
    data_val_int.t2000_Sa(iSubj,1) = mean(abs(actual_valence(tmp_idxSa,4)));

    % sort+average valence-ratings according to time*emotion-combination (in: reviewer response)
    bl_valence = data_raw.(subjects{iSubj}).val_t01;
    data_val_rat.bl_Af(iSubj,1) = mean(bl_valence(contains(data_raw.(subjects{iSubj}).cat_t01,'Af'),1));
    data_val_rat.bl_An(iSubj,1) = mean(bl_valence(contains(data_raw.(subjects{iSubj}).cat_t01,'An'),1));
    data_val_rat.bl_Ha(iSubj,1) = mean(bl_valence(contains(data_raw.(subjects{iSubj}).cat_t01,'Ha'),1));
    data_val_rat.bl_Sa(iSubj,1) = mean(bl_valence(contains(data_raw.(subjects{iSubj}).cat_t01,'Sa'),1));
    
    data_val_rat.t0_Af(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t02,'Af'),1));
    data_val_rat.t0_An(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t02,'An'),1));
    data_val_rat.t0_Ha(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t02,'Ha'),1));
    data_val_rat.t0_Sa(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t02,'Sa'),1));
    
    data_val_rat.t500_Af(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t500,'Af'),2));
    data_val_rat.t500_An(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t500,'An'),2));
    data_val_rat.t500_Ha(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t500,'Ha'),2));
    data_val_rat.t500_Sa(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t500,'Sa'),2));
    
    data_val_rat.t1000_Af(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t1000,'Af'),3));
    data_val_rat.t1000_An(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t1000,'An'),3));
    data_val_rat.t1000_Ha(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t1000,'Ha'),3));
    data_val_rat.t1000_Sa(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t1000,'Sa'),3));
    
    data_val_rat.t2000_Af(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t2000,'Af'),4));
    data_val_rat.t2000_An(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t2000,'An'),4));
    data_val_rat.t2000_Ha(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t2000,'Ha'),4));
    data_val_rat.t2000_Sa(iSubj,1) = mean(actual_valence(contains(data_raw.(subjects{iSubj}).cat_t2000,'Sa'),4));
end % subject loop

%% ****************** check assumption (normality) ***************************************
disp('     --> check for normality');
% call sub_function to check for normality of data and, if neccessary, apply
% 2-step-transformation. also, return statistics
[data_val_int4anova, stats_val_int_sw] = sub_shapiro_wilks(data_val_int);
[data_val_rat4anova, stats_val_rat_sw] = sub_shapiro_wilks(data_val_rat);

%% ****************** 2-way repeated measures ANOVA **************************************
disp('     --> perform 2way-repeated-measures-ANOVA');
% --- for valence intensity ---
% Create the within table
fact_timeshift = {'t0','t0','t0','t0','t500','t500','t500','t500','t1000','t1000','t1000','t1000','t2000','t2000','t2000','t2000'}';
fact_emotion = {'Af','An','Ha','Sa','Af','An','Ha','Sa','Af','An','Ha','Sa','Af','An','Ha','Sa'}';
WithinStructure = table(fact_timeshift, fact_emotion, 'VariableNames', {'TimeShift','Emotion'});

% call sub_function and perform
[stats_val_int_ranovatable,...
 stats_val_int_posthoc_TS,...
 stats_val_int_posthoc_EMO,...
 stats_val_int_posthoc_TSbyEMO] = sub_2way_rmANOVA(data_val_int4anova,WithinStructure);

% --- for valence ratings including baseline ---
% Create the within table
fact_timeshift = {'bl','bl','bl','bl','t0','t0','t0','t0','t500','t500','t500','t500','t1000','t1000','t1000','t1000','t2000','t2000','t2000','t2000'}';
fact_emotion = {'Af','An','Ha','Sa','Af','An','Ha','Sa','Af','An','Ha','Sa','Af','An','Ha','Sa','Af','An','Ha','Sa'}';
WithinStructure = table(fact_timeshift, fact_emotion, 'VariableNames', {'TimeShift','Emotion'});

% call sub_function and perform
[stats_val_rat_ranovatable,...
 stats_val_rat_posthoc_TS,...
 stats_val_rat_posthoc_EMO,...
 stats_val_rat_posthoc_TSbyEMO] = sub_2way_rmANOVA(data_val_rat4anova,WithinStructure);

%% ****************** plotting figures ***************************************************
% style ----------------------------------------------------------------------------------
s.background_color = 'w';

s.faceColor = [.1 .1 .1;...
               .3 .3 .3;...
               .5 .5 .5;...
               .7 .7 .7];

s.violinWidth = .4;
s.alpha = .5;
s.boxColor = [0 0 0];
s.boxWidth = 0.05;
s.medianColor = [1 1 1];

s.sigLineCol = [0 0 0];
s.sigLineWidth = .5;

s.scatSize = 20;
s.scatColor = [0.2 0.2 0.2];
s.scatAlpha = 0.7;

s.EB_capsize = 0;
s.EB_width = 2;

s.sigAstSize = 14;

txt.timeshift = {'+0 ms','+500 ms','+1000 ms','+2000 ms'};
txt.emotions = {'affection','anger','happiness','sadness'};

% ----------------------------------------------------------------------------------------
%% figure *** valence intensity ratings --------------------------------------------------------
% ----------------------------------------------------------------------------------------
disp('     --> plotting figure: valence intensity ratings');
fh_val_int = figure();
set(fh_val_int,'Position',[140 50 800 800]);
set(fh_val_int,'Color',s.background_color);

s.sigLineFirstInt = .3;
s.sigLineInt = .15;
s.sigLineStarDist = .05;
s.sigLineStarYOffset = -.04;

% maineffect TIMESHIFT -------------------------------------------------------------------
subplot(2,2,1); cla;
box off; hold on;

% get data
plt_t0 =    mean([data_val_int.t0_Af data_val_int.t0_An data_val_int.t0_Ha data_val_int.t0_Sa],2);
plt_t500 =  mean([data_val_int.t500_Af data_val_int.t500_An data_val_int.t500_Ha data_val_int.t500_Sa],2);
plt_t1000 = mean([data_val_int.t1000_Af data_val_int.t1000_An data_val_int.t1000_Ha data_val_int.t1000_Sa],2);
plt_t2000 = mean([data_val_int.t2000_Af data_val_int.t2000_An data_val_int.t2000_Ha data_val_int.t2000_Sa],2);
plt_data = [plt_t0,plt_t500,plt_t1000,plt_t2000];

% violin plot
for ts = 1:4
    vp(ts) = Violin(plt_data(:,ts), ts,...
                'Width',s.violinWidth,...
                'ViolinColor',s.faceColor(ts,:),...
                'ViolinAlpha',s.alpha,...
                'BoxColor',s.boxColor,...
                'BoxWidth',s.boxWidth,...
                'MedianColor',s.medianColor,...
                'ShowMean',true); %#ok<*SAGROW>

    % change scatter_point_size
    vp(ts).ScatterPlot.SizeData = s.scatSize;
    vp(ts).ScatterPlot.MarkerFaceColor = s.scatColor;
    vp(ts).ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
end

% significance bars
sigN = sum(stats_val_int_posthoc_TS.pValue < 0.05)/2;
sigLineMaxVal = max(plt_data(:));
sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
sigLineI = 1;
for i = 1:3
    for j = 4:-1:i+1
        cmp_ind = contains(stats_val_int_posthoc_TS.TimeShift_1,orderTimeShifts{i}) & contains(stats_val_int_posthoc_TS.TimeShift_2,orderTimeShifts{j});
        cmp_pVal = stats_val_int_posthoc_TS.pValue(cmp_ind);
        
        if cmp_pVal < 0.05
            ast = sub_getStars(cmp_pVal);
            line([i j],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
            text(j + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
            sigLineI = sigLineI + 1;
        end
    end
end

% finetuning
xlim([0.3 4.7]);
ylim([0 max([ceil(max(plt_data(:))), ceil(sigLineHeight)])]);
ax1 = gca;
ax1.XAxis.TickLength = [0 0];
ax1.YGrid = 'on';
set(ax1,...
    'XTick',1:4,...
    'XTickLabelRotation',20,...
    'XTickLabel',txt.timeshift,...
    'FontSize',12)
ylabel('valence intensity (absolute values)','FontSize',16);

% maineffect Emotion -------------------------------------------------------------------
subplot(2,2,2); cla;
box off; hold on;

% get data
plt_af = mean([data_val_int.t0_Af data_val_int.t500_Af data_val_int.t1000_Af data_val_int.t2000_Af],2);
plt_an = mean([data_val_int.t0_An data_val_int.t500_An data_val_int.t1000_An data_val_int.t2000_An],2);
plt_ha = mean([data_val_int.t0_Ha data_val_int.t500_Ha data_val_int.t1000_Ha data_val_int.t2000_Ha],2);
plt_sa = mean([data_val_int.t0_Sa data_val_int.t500_Sa data_val_int.t1000_Sa data_val_int.t2000_Sa],2);
plt_data = [plt_af,plt_an,plt_ha,plt_sa];

% violin plot
for ts = 1:4
    vp(ts) = Violin(plt_data(:,ts), ts,...
                'Width',s.violinWidth,...
                'ViolinColor',s.faceColor(ts,:),...
                'ViolinAlpha',s.alpha,...
                'BoxColor',s.boxColor,...
                'BoxWidth',s.boxWidth,...
                'MedianColor',s.medianColor,...
                'ShowMean',true); %#ok<*SAGROW>

	% change scatter_point_size
    vp(ts).ScatterPlot.SizeData = s.scatSize;
    vp(ts).ScatterPlot.MarkerFaceColor = s.scatColor;
    vp(ts).ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
end

% significance bars
sigN = sum(stats_val_int_posthoc_EMO.pValue < 0.05)/2;
sigLineMaxVal = max(plt_data(:));
sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
sigLineI = 1;
for i = 1:3
    for j = 4:-1:i+1
        cmp_ind = contains(stats_val_int_posthoc_EMO.Emotion_1,orderEmotions{i}) & contains(stats_val_int_posthoc_EMO.Emotion_2,orderEmotions{j});
        cmp_pVal = stats_val_int_posthoc_EMO.pValue(cmp_ind);
        
        if cmp_pVal < 0.05
            ast = sub_getStars(cmp_pVal);
            line([i j],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
            text(j + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
            sigLineI = sigLineI + 1;
        end
    end
end

% finetuning
xlim([0.3 4.7]);
ylim([0 max([ceil(max(plt_data(:))), ceil(sigLineHeight)])]);
ax2 = gca;
ax2.XAxis.TickLength = [0 0];
ax2.YGrid = 'on';
set(ax2,...
    'XTick',1:4,...
    'XTickLabel',txt.emotions,...
    'FontSize',12)
ylabel('valence intensity (absolute values)','FontSize',16);

% interaction TimeShift*Emotion ----------------------------------------------------------
subplot(2,2,[3 4]); cla;
box off; hold on;

% loop emotions and plot 4-timeshift-violins per emotion
mxLineHeight = 0;
for emo = 1:4
    if emo == 1
        xLoc = 1:4;
        xField = {'t0_Af','t500_Af','t1000_Af','t2000_Af'};
    elseif emo == 2
        xLoc = 6:9;
        xField = {'t0_An','t500_An','t1000_An','t2000_An'};
    elseif emo == 3
        xLoc = 11:14;
        xField = {'t0_Ha','t500_Ha','t1000_Ha','t2000_Ha'};
    elseif emo == 4
        xLoc = 16:19;
        xField = {'t0_Sa','t500_Sa','t1000_Sa','t2000_Sa'};
    end
    
    % draw violin for each timeshift
    mxVal = 0;
    for ts = 1:4
        vp(ts) = Violin(data_val_int.(xField{ts}), xLoc(ts),...
            'Width',s.violinWidth,...
            'ViolinColor',s.faceColor(ts,:),...
            'ViolinAlpha',s.alpha,...
            'BoxColor',s.boxColor,...
            'BoxWidth',s.boxWidth,...
            'MedianColor',s.medianColor,...
            'ShowMean',true); %#ok<*SAGROW>
        
        % change scatter_point_size
        vp(ts).ScatterPlot.SizeData = s.scatSize;
        vp(ts).ScatterPlot.MarkerFaceColor = s.scatColor;
        vp(ts).ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
        
        % save max value for sigLines
        mxVal = max([mxVal; data_val_int.(xField{ts})]);
    end
    
    % significance bars
    sigEmoInd = strcmp(stats_val_int_posthoc_TSbyEMO.Emotion,orderEmotions{emo});
    sigN = sum(stats_val_int_posthoc_TSbyEMO.pValue(sigEmoInd) < 0.05)/2;
    sigLineMaxVal = mxVal;
    sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
    sigLineI = 1;
    for i = 1:3
        for j = 4:-1:i+1
            cmp_ind = sigEmoInd & contains(stats_val_int_posthoc_TSbyEMO.TimeShift_1,orderTimeShifts{i}) & contains(stats_val_int_posthoc_TSbyEMO.TimeShift_2,orderTimeShifts{j});
            cmp_pVal = stats_val_int_posthoc_TSbyEMO.pValue(cmp_ind);
            
            if cmp_pVal < 0.05
                ast = sub_getStars(cmp_pVal);
                line([xLoc(i) xLoc(j)],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                text(xLoc(j) + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
                sigLineI = sigLineI + 1;
            end
        end
    end
    if ~isempty(sigLineHeight)
        mxLineHeight = max([mxLineHeight, sigLineHeight(1)]);
    end
end

% finetuning
xlim([0.3 19.7]);
ylim([0 max([1, ceil(mxLineHeight)])]);
ax3 = gca;
ax3.XAxis.TickLength = [0 0];
ax3.YGrid = 'on';
set(ax3,'XTick',[2.5 7.5 12.5 17.5],'XTickLabel',txt.emotions,'FontSize',12);
ylabel('valence intensity (absolute values)','FontSize',16);

lgd = [vp(1).ViolinPlot, vp(2).ViolinPlot, vp(3).ViolinPlot, vp(4).ViolinPlot];
lg = legend(lgd,txt.timeshift,'Location','NorthEastOutside');
title(lg,legendCaption,'FontSize',10);

% apply max_ylim to both subplots -------------------------------------------------------
ylim(ax1,[0 ceil(max([ax1.YLim ax2.YLim ax3.YLim]))]);
ylim(ax2,[0 ceil(max([ax1.YLim ax2.YLim ax3.YLim]))]);
ylim(ax3,[0 ceil(max([ax1.YLim ax2.YLim ax3.YLim]))]);

% figure-finetuning ----------------------------------------------------------------------
% add A) + B) + C)
annotation('textbox',[.06 .88 .1 .1],'String','A)','FontSize',14,'FontWeight','bold','EdgeColor',[1 1 1]);
annotation('textbox',[.50 .88 .1 .1],'String','B)','FontSize',14,'FontWeight','bold','EdgeColor',[1 1 1]);
annotation('textbox',[.06 .4 .1 .1],'String','C)','FontSize',14,'FontWeight','bold','EdgeColor',[1 1 1]);

% ----------------------------------------------------------------------------------------
%% figure *** valence ratings --------------------------------------------------------
% ----------------------------------------------------------------------------------------
disp('     --> plotting figure: valence ratings');
s.faceColor = [0 0 0;...
               .2 .2 .2;...
               .4 .4 .4;...
               .6 .6 .6;...
               .8 .8 .8];

s.scatSize = 12;

fh_val_rat = figure();
set(fh_val_rat,'Position',[140 50 1050 330]);
set(fh_val_rat,'Color',s.background_color);

s.sigLineFirstInt = .3;
s.sigLineInt = .15;
s.sigLineStarDist = .05;
s.sigLineStarYOffset = -.08;

% maineffect Emotion -------------------------------------------------------------------
subplot(1,4,1); cla;
box off; hold on;

% get data
plt_af = mean([data_val_rat.bl_Af data_val_rat.t0_Af data_val_rat.t500_Af data_val_rat.t1000_Af data_val_rat.t2000_Af],2);
plt_an = mean([data_val_rat.bl_An data_val_rat.t0_An data_val_rat.t500_An data_val_rat.t1000_An data_val_rat.t2000_An],2);
plt_ha = mean([data_val_rat.bl_Ha data_val_rat.t0_Ha data_val_rat.t500_Ha data_val_rat.t1000_Ha data_val_rat.t2000_Ha],2);
plt_sa = mean([data_val_rat.bl_Sa data_val_rat.t0_Sa data_val_rat.t500_Sa data_val_rat.t1000_Sa data_val_rat.t2000_Sa],2);
plt_data = [plt_af,plt_an,plt_ha,plt_sa];

% violin plot
for ts = 1:4
    vp(ts) = Violin(plt_data(:,ts), ts,...
                'Width',s.violinWidth,...
                'ViolinColor',s.faceColor(ts,:),...
                'ViolinAlpha',s.alpha,...
                'BoxColor',s.boxColor,...
                'BoxWidth',s.boxWidth,...
                'MedianColor',s.medianColor,...
                'ShowMean',true); %#ok<*SAGROW>

	% change scatter_point_size
    vp(ts).ScatterPlot.SizeData = s.scatSize;
    vp(ts).ScatterPlot.MarkerFaceColor = s.scatColor;
    vp(ts).ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
end

% significance bars
sigN = sum(stats_val_rat_posthoc_EMO.pValue < 0.05)/2;
sigLineMaxVal = max(plt_data(:));
sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
sigLineI = 1;
for i = 1:3
    for j = 4:-1:i+1
        cmp_ind = contains(stats_val_rat_posthoc_EMO.Emotion_1,orderEmotions{i}) & contains(stats_val_rat_posthoc_EMO.Emotion_2,orderEmotions{j});
        cmp_pVal = stats_val_rat_posthoc_EMO.pValue(cmp_ind);
        
        if cmp_pVal < 0.05
            ast = sub_getStars(cmp_pVal);
            line([i j],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
            text(j + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
            sigLineI = sigLineI + 1;
        end
    end
end

% finetuning
xlim([0.3 4.7]);
ylim([-5 5]);
ax2 = gca;
ax2.XAxis.TickLength = [0 0];
ax2.YGrid = 'on';
set(ax2,...
    'XTick',1:4,...
    'XTickLabel',txt.emotions,...
    'FontSize',12, ...
    'YTick',-5:1:5);
line(xlim,[0 0],'Color','k','LineWidth',.5);
ylabel('valence','FontSize',16);

% interaction TimeShift*Emotion ----------------------------------------------------------
subplot(1,4,[2 3 4]); cla;
box off; hold on;

% loop emotions and plot 4-timeshift-violins per emotion
mxLineHeight = 0;
for emo = 1:4
    if emo == 1
        xLoc = 1:5;
        xField = {'bl_Af','t0_Af','t500_Af','t1000_Af','t2000_Af'};
    elseif emo == 2
        xLoc = 7:11;
        xField = {'bl_An','t0_An','t500_An','t1000_An','t2000_An'};
    elseif emo == 3
        xLoc = 13:17;
        xField = {'bl_Ha','t0_Ha','t500_Ha','t1000_Ha','t2000_Ha'};
    elseif emo == 4
        xLoc = 19:23;
        xField = {'bl_Sa','t0_Sa','t500_Sa','t1000_Sa','t2000_Sa'};
    end
    
    % draw violin for each timeshift
    mxVal = -inf;
    for ts = 1:5
        vp(ts) = Violin(data_val_rat.(xField{ts}), xLoc(ts),...
            'Width',s.violinWidth,...
            'ViolinColor',s.faceColor(ts,:),...
            'ViolinAlpha',s.alpha,...
            'BoxColor',s.boxColor,...
            'BoxWidth',s.boxWidth,...
            'MedianColor',s.medianColor,...
            'ShowMean',true); %#ok<*SAGROW>
        
        % change scatter_point_size
        vp(ts).ScatterPlot.SizeData = s.scatSize;
        vp(ts).ScatterPlot.MarkerFaceColor = s.scatColor;
        vp(ts).ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
        
        % save max value for sigLines
        mxVal = max([mxVal; data_val_rat.(xField{ts})]);
    end
    
    % significance bars
    sigEmoInd = strcmp(stats_val_rat_posthoc_TSbyEMO.Emotion,orderEmotions{emo});
    sigN = sum(stats_val_rat_posthoc_TSbyEMO.pValue(sigEmoInd) < 0.05)/2;
    sigLineMaxVal = mxVal;
    sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
    sigLineI = 1;
    for i = 1:4
        for j = 5:-1:i+1
            cmp_ind = sigEmoInd & contains(stats_val_rat_posthoc_TSbyEMO.TimeShift_1,orderTimeShifts_bl{i}) & contains(stats_val_rat_posthoc_TSbyEMO.TimeShift_2,orderTimeShifts_bl{j});
            cmp_pVal = stats_val_rat_posthoc_TSbyEMO.pValue(cmp_ind);
            
            if cmp_pVal < 0.05
                ast = sub_getStars(cmp_pVal);
                line([xLoc(i) xLoc(j)],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                text(xLoc(j) + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
                sigLineI = sigLineI + 1;
            end
        end
    end
    if ~isempty(sigLineHeight)
        mxLineHeight = max([mxLineHeight, sigLineHeight(1)]);
    end
end

% finetuning
xlim([0.3 23.7]);
ylim([-5 5]);
ax3 = gca;
ax3.XAxis.TickLength = [0 0];
ax3.YGrid = 'on';
set(ax3,'XTick',[3 9 15 21],...
    'XTickLabel',txt.emotions,...
    'FontSize',12,...
    'YTick',-5:1:5);
line(xlim,[0 0],'Color','k','LineWidth',.5);
ylabel('valence','FontSize',16);

lgd = [vp(1).ViolinPlot, vp(2).ViolinPlot, vp(3).ViolinPlot, vp(4).ViolinPlot, vp(5).ViolinPlot];
lg = legend(lgd,['baseline',txt.timeshift],'Location','NorthEastOutside');
title(lg,legendCaption,'FontSize',10);

% figure-finetuning ----------------------------------------------------------------------
% add A) + B)
annotation('textbox',[.08 .88 .1 .1],'String','A)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.295 .88 .1 .1],'String','B)','FontSize',14,'FontWeight','bold','EdgeColor','none');

%% ****************** save data and figures **********************************************
if saving_data == true
    disp('     --> saving analysis results');
    % convert class of specific table_coloumns (don't know why, but neccessary)
    stats_val_int_ranovatable.F = double(stats_val_int_ranovatable.F);
    stats_val_int_ranovatable.pValue = double(stats_val_int_ranovatable.pValue);
    stats_val_int_ranovatable.pValueGG = double(stats_val_int_ranovatable.pValueGG);
    stats_val_int_ranovatable.pValueHF = double(stats_val_int_ranovatable.pValueHF);
    stats_val_int_ranovatable.pValueLB = double(stats_val_int_ranovatable.pValueLB);
    
    stats_val_rat_ranovatable.F = double(stats_val_rat_ranovatable.F);
    stats_val_rat_ranovatable.pValue = double(stats_val_rat_ranovatable.pValue);
    stats_val_rat_ranovatable.pValueGG = double(stats_val_rat_ranovatable.pValueGG);
    stats_val_rat_ranovatable.pValueHF = double(stats_val_rat_ranovatable.pValueHF);
    stats_val_rat_ranovatable.pValueLB = double(stats_val_rat_ranovatable.pValueLB);
    
    % data_tables
    stats_val_int_sw.Properties.DimensionNames{1} = 'condition';
    stats_val_rat_sw.Properties.DimensionNames{1} = 'condition';

    writetable(struct2table(data_val_int),fullfile(folder.data,'AnalysisC_valence_intensity_subject_data.csv'));
	writetable(stats_val_int_ranovatable,fullfile(folder.data,'AnalysisC_valence_intensity_stats_anova.csv'),'WriteRowNames',true);
	writetable(stats_val_int_posthoc_TS,fullfile(folder.data,'AnalysisC_valence_intensity_stats_postHoc_timeshift.csv'));
	writetable(stats_val_int_posthoc_EMO,fullfile(folder.data,'AnalysisC_valence_intensity_stats_postHoc_emotion.csv'));
	writetable(stats_val_int_posthoc_TSbyEMO,fullfile(folder.data,'AnalysisC_valence_intensity_stats_postHoc_timeshiftBYemotion.csv'));
	writetable(stats_val_int_sw,fullfile(folder.data,'AnalysisC_valence_intensity_Shapiro_Wilks.csv'),'WriteRowNames',true);
    
    writetable(struct2table(data_val_rat),fullfile(folder.data,'AnalysisC_valence_ratings_subject_data.csv'));
	writetable(stats_val_rat_ranovatable,fullfile(folder.data,'AnalysisC_valence_ratings_stats_anova.csv'),'WriteRowNames',true);
	writetable(stats_val_rat_posthoc_TS,fullfile(folder.data,'AnalysisC_valence_ratings_stats_postHoc_timeshift.csv'));
	writetable(stats_val_rat_posthoc_EMO,fullfile(folder.data,'AnalysisC_valence_ratings_stats_postHoc_emotion.csv'));
	writetable(stats_val_rat_posthoc_TSbyEMO,fullfile(folder.data,'AnalysisC_valence_ratings_stats_postHoc_timeshiftBYemotion.csv'));
	writetable(stats_val_rat_sw,fullfile(folder.data,'AnalysisC_valence_ratings_Shapiro_Wilks.csv'),'WriteRowNames',true);
end

if saving_figs == true
    disp('     --> saving figures');

    savefig(fh_val_int,fullfile(folder.figs,'AnalysisC_fig5_valence_intensity'));
    exportgraphics(fh_val_int,fullfile(folder.figs,'AnalysisC_fig5_valence_intensity.pdf'),'ContentType','vector');

    savefig(fh_val_rat,fullfile(folder.figs,'AnalysisC_figS2_valence_ratings'));
    exportgraphics(fh_val_rat,fullfile(folder.figs,'AnalysisC_figS2_valence_ratings.pdf'),'ContentType','vector');
end

%% ****************** displaying anova results *******************************
disp('     --> displaying statistical results of rmANOVA');
% valence intensity -----------------------------------------------------------------
switch stats_val_int_ranovatable.correction{3}
    case 'none'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_val_int_ranovatable.mauchlyDF(3),...
                                        stats_val_int_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(3)));
                
        this_df1_TS = sprintf('%d',stats_val_int_ranovatable.DF(3));
        this_df2_TS = sprintf('%d',stats_val_int_ranovatable.DF(4));
        this_F_TS = stats_val_int_ranovatable.F(3);
        this_p_TS = stats_val_int_ranovatable.pValue(3);
        this_pETAsq_TS = stats_val_int_ranovatable.pEtaSq(3);
        
    case 'GG'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_val_int_ranovatable.mauchlyDF(3),...
                                        stats_val_int_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(3)),...
                                        stats_val_int_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_val_int_ranovatable.dfGG(3));
        this_df2_TS = sprintf('%.2f',stats_val_int_ranovatable.dfGG(4));
        this_F_TS = stats_val_int_ranovatable.F(3);
        this_p_TS = stats_val_int_ranovatable.pValueGG(3);
        this_pETAsq_TS = stats_val_int_ranovatable.pEtaSq(3);
        
    case 'HF'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_val_int_ranovatable.mauchlyDF(3),...
                                        stats_val_int_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(3)),...
                                        stats_val_int_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_val_int_ranovatable.dfHF(3));
        this_df2_TS = sprintf('%.2f',stats_val_int_ranovatable.dfHF(4));
        this_F_TS = stats_val_int_ranovatable.F(3);
        this_p_TS = stats_val_int_ranovatable.pValueHF(3);
        this_pETAsq_TS = stats_val_int_ranovatable.pEtaSq(3);
end

switch stats_val_int_ranovatable.correction{5}
    case 'none'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_val_int_ranovatable.mauchlyDF(5),...
                                        stats_val_int_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(5)));
        
        this_df1_EMO = sprintf('%d',stats_val_int_ranovatable.DF(5));
        this_df2_EMO = sprintf('%d',stats_val_int_ranovatable.DF(6));
        this_F_EMO = stats_val_int_ranovatable.F(5);
        this_p_EMO = stats_val_int_ranovatable.pValue(5);
        this_pETAsq_EMO = stats_val_int_ranovatable.pEtaSq(5);
        
    case 'GG'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_val_int_ranovatable.mauchlyDF(5),...
                                        stats_val_int_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(5)),...
                                        stats_val_int_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_val_int_ranovatable.dfGG(5));
        this_df2_EMO = sprintf('%.2f',stats_val_int_ranovatable.dfGG(6));
        this_F_EMO = stats_val_int_ranovatable.F(5);
        this_p_EMO = stats_val_int_ranovatable.pValueGG(5);
        this_pETAsq_EMO = stats_val_int_ranovatable.pEtaSq(5);
        
    case 'HF'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_val_int_ranovatable.mauchlyDF(5),...
                                        stats_val_int_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(5)),...
                                        stats_val_int_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_val_int_ranovatable.dfHF(5));
        this_df2_EMO = sprintf('%.2f',stats_val_int_ranovatable.dfHF(6));
        this_F_EMO = stats_val_int_ranovatable.F(5);
        this_p_EMO = stats_val_int_ranovatable.pValueHF(5);
        this_pETAsq_EMO = stats_val_int_ranovatable.pEtaSq(5);
end

switch stats_val_int_ranovatable.correction{7}
    case 'none'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                            stats_val_int_ranovatable.mauchlyDF(7),...
                                            stats_val_int_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(7)));
        
        this_df1_TSbyEMO = sprintf('%d',stats_val_int_ranovatable.DF(7));
        this_df2_TSbyEMO = sprintf('%d',stats_val_int_ranovatable.DF(8));
        this_F_TSbyEMO = stats_val_int_ranovatable.F(7);
        this_p_TSbyEMO = stats_val_int_ranovatable.pValue(7);
        this_pETAsq_TSbyEMO = stats_val_int_ranovatable.pEtaSq(7);

    case 'GG'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                            stats_val_int_ranovatable.mauchlyDF(7),...
                                            stats_val_int_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(7)),...
                                            stats_val_int_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_val_int_ranovatable.dfGG(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_val_int_ranovatable.dfGG(8));
        this_F_TSbyEMO = stats_val_int_ranovatable.F(7);
        this_p_TSbyEMO = stats_val_int_ranovatable.pValueGG(7);
        this_pETAsq_TSbyEMO = stats_val_int_ranovatable.pEtaSq(7);
        
    case 'HF'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                            stats_val_int_ranovatable.mauchlyDF(7),...
                                            stats_val_int_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_val_int_ranovatable.mauchlyPvalue(7)),...
                                            stats_val_int_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_val_int_ranovatable.dfHF(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_val_int_ranovatable.dfHF(8));
        this_F_TSbyEMO = stats_val_int_ranovatable.F(7);
        this_p_TSbyEMO = stats_val_int_ranovatable.pValueHF(7);
        this_pETAsq_TSbyEMO = stats_val_int_ranovatable.pEtaSq(7);    
end

disp(' ');
disp('         --- ANOVA results: valence intensity (absolute values) ---------------------------------------');
fprintf('             main effect -> TimeShift\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TS,this_df1_TS,this_df2_TS,this_F_TS,sub_getPString(this_p_TS),this_pETAsq_TS)
fprintf('             main effect -> Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_EMO,this_df1_EMO,this_df2_EMO,this_F_EMO,sub_getPString(this_p_EMO),this_pETAsq_EMO)
fprintf('             interaction -> TimeShift * Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TSbyEMO,this_df1_TSbyEMO,this_df2_TSbyEMO,this_F_TSbyEMO,sub_getPString(this_p_TSbyEMO),this_pETAsq_TSbyEMO)

% valence rating (reviewer response)----------------------------------------------------
switch stats_val_rat_ranovatable.correction{3}
    case 'none'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_val_rat_ranovatable.mauchlyDF(3),...
                                        stats_val_rat_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(3)));
                
        this_df1_TS = sprintf('%d',stats_val_rat_ranovatable.DF(3));
        this_df2_TS = sprintf('%d',stats_val_rat_ranovatable.DF(4));
        this_F_TS = stats_val_rat_ranovatable.F(3);
        this_p_TS = stats_val_rat_ranovatable.pValue(3);
        this_pETAsq_TS = stats_val_rat_ranovatable.pEtaSq(3);
        
    case 'GG'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_val_rat_ranovatable.mauchlyDF(3),...
                                        stats_val_rat_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(3)),...
                                        stats_val_rat_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_val_rat_ranovatable.dfGG(3));
        this_df2_TS = sprintf('%.2f',stats_val_rat_ranovatable.dfGG(4));
        this_F_TS = stats_val_rat_ranovatable.F(3);
        this_p_TS = stats_val_rat_ranovatable.pValueGG(3);
        this_pETAsq_TS = stats_val_rat_ranovatable.pEtaSq(3);
        
    case 'HF'
        this_correction_TS = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_val_rat_ranovatable.mauchlyDF(3),...
                                        stats_val_rat_ranovatable.mauchlyChiSq(3),...
                                        sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(3)),...
                                        stats_val_rat_ranovatable.epsilonGG(3));
        
        this_df1_TS = sprintf('%.2f',stats_val_rat_ranovatable.dfHF(3));
        this_df2_TS = sprintf('%.2f',stats_val_rat_ranovatable.dfHF(4));
        this_F_TS = stats_val_rat_ranovatable.F(3);
        this_p_TS = stats_val_rat_ranovatable.pValueHF(3);
        this_pETAsq_TS = stats_val_rat_ranovatable.pEtaSq(3);
end

switch stats_val_rat_ranovatable.correction{5}
    case 'none'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                        stats_val_rat_ranovatable.mauchlyDF(5),...
                                        stats_val_rat_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(5)));
        
        this_df1_EMO = sprintf('%d',stats_val_rat_ranovatable.DF(5));
        this_df2_EMO = sprintf('%d',stats_val_rat_ranovatable.DF(6));
        this_F_EMO = stats_val_rat_ranovatable.F(5);
        this_p_EMO = stats_val_rat_ranovatable.pValue(5);
        this_pETAsq_EMO = stats_val_rat_ranovatable.pEtaSq(5);
        
    case 'GG'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                        stats_val_rat_ranovatable.mauchlyDF(5),...
                                        stats_val_rat_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(5)),...
                                        stats_val_rat_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_val_rat_ranovatable.dfGG(5));
        this_df2_EMO = sprintf('%.2f',stats_val_rat_ranovatable.dfGG(6));
        this_F_EMO = stats_val_rat_ranovatable.F(5);
        this_p_EMO = stats_val_rat_ranovatable.pValueGG(5);
        this_pETAsq_EMO = stats_val_rat_ranovatable.pEtaSq(5);
        
    case 'HF'
        this_correction_EMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                        stats_val_rat_ranovatable.mauchlyDF(5),...
                                        stats_val_rat_ranovatable.mauchlyChiSq(5),...
                                        sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(5)),...
                                        stats_val_rat_ranovatable.epsilonGG(5));
        
        this_df1_EMO = sprintf('%.2f',stats_val_rat_ranovatable.dfHF(5));
        this_df2_EMO = sprintf('%.2f',stats_val_rat_ranovatable.dfHF(6));
        this_F_EMO = stats_val_rat_ranovatable.F(5);
        this_p_EMO = stats_val_rat_ranovatable.pValueHF(5);
        this_pETAsq_EMO = stats_val_rat_ranovatable.pEtaSq(5);
end

switch stats_val_rat_ranovatable.correction{7}
    case 'none'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s',...
                                            stats_val_rat_ranovatable.mauchlyDF(7),...
                                            stats_val_rat_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(7)));
        
        this_df1_TSbyEMO = sprintf('%d',stats_val_rat_ranovatable.DF(7));
        this_df2_TSbyEMO = sprintf('%d',stats_val_rat_ranovatable.DF(8));
        this_F_TSbyEMO = stats_val_rat_ranovatable.F(7);
        this_p_TSbyEMO = stats_val_rat_ranovatable.pValue(7);
        this_pETAsq_TSbyEMO = stats_val_rat_ranovatable.pEtaSq(7);

    case 'GG'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                            stats_val_rat_ranovatable.mauchlyDF(7),...
                                            stats_val_rat_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(7)),...
                                            stats_val_rat_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_val_rat_ranovatable.dfGG(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_val_rat_ranovatable.dfGG(8));
        this_F_TSbyEMO = stats_val_rat_ranovatable.F(7);
        this_p_TSbyEMO = stats_val_rat_ranovatable.pValueGG(7);
        this_pETAsq_TSbyEMO = stats_val_rat_ranovatable.pEtaSq(7);
        
    case 'HF'
        this_correction_TSbyEMO = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                            stats_val_rat_ranovatable.mauchlyDF(7),...
                                            stats_val_rat_ranovatable.mauchlyChiSq(7),...
                                            sub_getPString(stats_val_rat_ranovatable.mauchlyPvalue(7)),...
                                            stats_val_rat_ranovatable.epsilonGG(7));
        
        this_df1_TSbyEMO = sprintf('%.2f',stats_val_rat_ranovatable.dfHF(7));
        this_df2_TSbyEMO = sprintf('%.2f',stats_val_rat_ranovatable.dfHF(8));
        this_F_TSbyEMO = stats_val_rat_ranovatable.F(7);
        this_p_TSbyEMO = stats_val_rat_ranovatable.pValueHF(7);
        this_pETAsq_TSbyEMO = stats_val_rat_ranovatable.pEtaSq(7);    
end

disp(' ');
disp('         --- ANOVA results: valence ratings ---------------------------------------');
fprintf('             main effect -> TimeShift\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TS,this_df1_TS,this_df2_TS,this_F_TS,sub_getPString(this_p_TS),this_pETAsq_TS)
fprintf('             main effect -> Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_EMO,this_df1_EMO,this_df2_EMO,this_F_EMO,sub_getPString(this_p_EMO),this_pETAsq_EMO)
fprintf('             interaction -> TimeShift * Emotion\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
    this_correction_TSbyEMO,this_df1_TSbyEMO,this_df2_TSbyEMO,this_F_TSbyEMO,sub_getPString(this_p_TSbyEMO),this_pETAsq_TSbyEMO)

%% finished
disp(' ');
disp('>>> done <<<');
