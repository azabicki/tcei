%% Analysis D: exploratory analysis, investigating influence of temporal offset on kinematic
%              parameters obtained by 'SAMI' toolbox, and testing associations with perceived
%              emotion ratings of temporally decoupled sequences.
%              *** NOTE *** Because temporal offset only influences perception of AFFECTION and
%              ANGER trials, the forthcoming script perform analysis only on these two
%              emotional categories.
%
% code that leads to
%   - figure 6 in the manuscript
%   - figures A.3 in Appendix A
%   - statistical values (see output in command window), shown in table A.5 in Appendix A
%
% used References:
% Girden, E. R. (1992). ANOVA: Repeated measures. Sage university papers. Quantitative
%       applications in the social sciences: no. 07-084. Newbury Park, Calif.: Sage Publications.
% Templeton, G.F. (2011). A Two-Step Approach for Transforming Continuous Variables to
%       Normal: Implications and Recommendations for IS Research. Communications of the AIS, Vol. 28, Article 4.
% Tomczak, M., & Tomczak, E. (2014). The need to report effect size estimates revisited.
%       An overview of some recommended measures of effect size. Trends Sport Sci. 1(21), 19�25.
% Zabicki,A. & Keck, J. (2021). SAMI: Similarity Analysis of Human Movements and Interactions (v0.1.0).
%       Zenodo. http://doi.org/10.5281/zenodo.4764552

clear all; close all;
disp(' *** Analysis D: exploratory anaylsis investigating influence of temporal offset on kinematics');

%% ***** options *************************************************************************
saving_data = true;
saving_figs = true;

% folder
folder.data = fullfile('..','analysis_results');
folder.figs = fullfile('..','analysis_figures');
folder.sami_root = fullfile('..','analysis_results','SAMI_results');
folder.c3d = fullfile('..','stimuli_c3d');
folder.sami_toolbox = fullfile('..','SAMI-0.1.0','sami_toolbox');

%% ***** init vars ***********************************************************************
data_subjects = sub_load_txt_files(folder); % load experimental data
nEmotions = 4;
nStimPerEmo = 17;
nTimeShifts = 4;
eoi = {'Af','An'}; % Emotions of Interest
eoi_full = {'Affection','Anger'}; % for plotting purposes

% order of emotional categories and timeshifts
orderEmotions = {'Af','An','Ha','Sa'};
orderTimeShifts = {'t0','t500','t1000','t2000'};
legendCaption = 'temporal offset';

% text snippets for figures
txt.timeshift = {'+0 ms','+500 ms','+1000 ms','+2000 ms'};
txt.emotions = {'affection','anger','happiness','sadness'};
txt.features = {'IP_distance_avg','IPDavg';...
                'IP_distance_std','IPDvar';...
                'IP_orientation_average','IPOavg';...
                'IP_orientation_balance','IPObal';...
                'corr_dist_velocity','CorrDistVel';...
                'corr_dist_acceleration','CorrDistAcc';...
                'corr_dist_limb_contraction','CorrDistLC';...
                'corr_dist_volume','CorrDistLV';...
                'sync_vel','SyncVel';...
                'sync_acc','SyncAcc';...
                'motion_energy_balance','MEB';...
                'personalspace','PS'};

%% ***** SAMI: calculate interaction specific (itx) features *****************************
[feat, c3dData] = sub_SAMI_toolbox(folder);

%% ***** preprocess data *****************************************************************
% init vars
nFeat = numel(feat.Af_t0);
cat = fieldnames(feat);
nCat = numel(cat);

% sort data for each feature-set
units = cell(nFeat,1);
features = cell(nFeat,1);
fisherZ = false(nFeat,1);
data = struct();
for f = 1:nFeat
    features{f} = feat.(cat{1})(f).name;
    units{f} = feat.(cat{1})(f).unit;
    fisherZ(f) = feat.(cat{1})(f).fisherZ4paramTesting;

    for c = 1:nCat
         if fisherZ(f) == 1
             data.(features{f}).(cat{c}) = tanh(mean(atanh(feat.(cat{c})(f).fSet),1))';
         else
            data.(features{f}).(cat{c}) = mean(feat.(cat{c})(f).fSet,1)';
         end            
    end
end

% delete unnecessary features + update nFeat
data = rmfield(data, 'sync_acc_arms');
data = rmfield(data, 'sync_acc_trunc');
data = rmfield(data, 'sync_vel_arms');
data = rmfield(data, 'sync_vel_trunc');
units(contains(features,{'sync_acc_trunc','sync_acc_arms','sync_vel_trunc','sync_vel_arms'})) = [];
fisherZ(contains(features,{'sync_acc_trunc','sync_acc_arms','sync_vel_trunc','sync_vel_arms'})) = [];
features(contains(features,{'sync_acc_trunc','sync_acc_arms','sync_vel_trunc','sync_vel_arms'})) = [];
nFeat = numel(features);

%% ***** do statistical testing for each feature *****************************************
stats = struct();
for f = 1:nFeat
    disp(['  --> statistical routines for feat: ' features{f} ' (' num2str(f) '/' num2str(nFeat) ')']);

    % loop both *emotions of interest* [affection/anger]
    for e = eoi
        disp(['     --> analyse *' e{1} '* sequences']);

        % get current data
        data_feat.t0 = data.(features{f}).([e{1} '_t0']);
        data_feat.t500 = data.(features{f}).([e{1} '_t500']);
        data_feat.t1000 = data.(features{f}).([e{1} '_t1000']);
        data_feat.t2000 = data.(features{f}).([e{1} '_t2000']);

        % *************** perform fisher-z-transformation for correlational data *********
        if fisherZ(f)
            data_feat.t0 = atanh(data_feat.t0);
            data_feat.t500 = atanh(data_feat.t500);
            data_feat.t1000 = atanh(data_feat.t1000);
            data_feat.t2000 = atanh(data_feat.t2000);
        end

        % *************** check assumption: normality ************************************
        disp('        --> check for normality');
        % call sub_function to check for normality of data and, if neccessary, apply
        % 2-step-transformation. also, return statistics
        [data_feat4anova, stats.(features{f}).(e{1}).feat_sw] = sub_shapiro_wilks(data_feat);

        % *************** one-way rm-ANOVA ***********************************************
        disp('        --> perform one-way-rm-ANOVA');
        % Create the between/within table
        WithinStructure = table(orderTimeShifts', 'VariableNames', {'TimeShift'});
        % call sub_function and perform
        [stats.(features{f}).(e{1}).ranovatable,...
            stats.(features{f}).(e{1}).posthoc] = sub_1way_rmANOVA(data_feat4anova,WithinStructure);

        % *************** calc kin. data for correct/incorrect trials '*******************
        disp('        --> calculate kinematic features for (in)correct trials');
        % call sub_function to get per person averaged kinematic features, seperated by
        % correct and incorrect recognized emotions
        [stats.(features{f}).(e{1}).kinematics.correct,...
            stats.(features{f}).(e{1}).kinematics.incorrect] = sub_calc_kin_features(data_subjects,features{f},e{1},feat,c3dData,orderEmotions);

        % *************** test kin. data of correct/incorrect trials for differences *****
        disp('        --> test kinematic features for differences between correct-vs-incorrect trials');
        % Wilcoxon signed rank test, due to violations of normality and equality of
        % variances in almost every feature
        stats.(features{f}).(e{1}).kinematics_signedRank = sub_wilcoxon(stats.(features{f}).(e{1}).kinematics.correct, ...
            stats.(features{f}).(e{1}).kinematics.incorrect);
    end % eoi loop
end % feature loop

%% ****************** plotting figures ***************************************************
% style ----------------------------------------------------------------------------------
s.background_color = 'w';

s.faceColor = [.1 .1 .1;...
    .3 .3 .3;...
    .5 .5 .5;...
    .7 .7 .7];

s.violinWidth = .4;
s.alpha = .5;
s.boxColor = [0 0 0];
s.boxWidth = 0.03;
s.medianColor = [1 1 1];

s.sigLineCol = [0 0 0];
s.sigLineWidth = 0.5;

s.sigAstSize = 20;

s.scatSize = 20;
s.scatColor = [0.2 0.2 0.2];
s.scatAlpha = 0.7;

s.txtSizeAnova = 7;
s.txtSizeYLabel = 16;
s.axesFontSize = 12;

% ----------------------------------------------------------------------------------------
%% figure 6 *** effects on kinematic features ---------------------------------------------
% ----------------------------------------------------------------------------------------
disp('  --> plotting figure 6: effects of ''motion energy balance''');
% init figure
fh_kin = figure();

set(fh_kin,'Position',[140 50 500 930]);
set(fh_kin,'Color',s.background_color);

% MEB --- ANOVA results ------------------------------------------------------------------
subplot(3,3,[1 2]); cla;
box off; hold on;

% get current data
plt_anova(:,1) = data.motion_energy_balance.Af_t0;
plt_anova(:,2) = data.motion_energy_balance.Af_t500;
plt_anova(:,3) = data.motion_energy_balance.Af_t1000;
plt_anova(:,4) = data.motion_energy_balance.Af_t2000;

% violin plot
for ts = 1:4
    vh = Violin(plt_anova(:,ts), ts,...
        'Width',s.violinWidth,...
        'ViolinColor',s.faceColor(ts,:),...
        'ViolinAlpha',s.alpha,...
        'BoxColor',s.boxColor,...
        'BoxWidth',s.boxWidth,...
        'MedianColor',s.medianColor,...
        'ShowMean',true); %#ok<*SAGROW>

    % change scatter_point_size
    vh.ScatterPlot.SizeData = s.scatSize;
    vh.ScatterPlot.MarkerFaceColor = s.scatColor;
    vh.ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
end

% finetuning
xlim([0.3 4.7]);
ylim([0.35 1]);
axA = gca;
axA.XAxis.TickLength = [0 0];
axA.YGrid = 'on';
set(axA,'XTick',[],...
        'YTick',[.4 .6 .8 1],...
        'fontsize',s.axesFontSize);
ylabel('MEB [AU]','FontSize',s.txtSizeYLabel);

% MEB --- Wilcoxon signed rank test results ------------------------------------------------------
subplot(3,3,3); cla;
box off; hold on;

% get current data
plt_wilcoxon(:,1) = stats.motion_energy_balance.Af.kinematics.correct;
plt_wilcoxon(:,2) = stats.motion_energy_balance.Af.kinematics.incorrect;

% violin plot
for ts = 1:2
    vh = Violin(plt_wilcoxon(:,ts), ts,...
        'Width',s.violinWidth,...
        'ViolinColor',s.faceColor(2*ts-1,:),...
        'ViolinAlpha',s.alpha,...
        'BoxColor',s.boxColor,...
        'BoxWidth',s.boxWidth,...
        'MedianColor',s.medianColor,...
        'ShowMean',true); %#ok<*SAGROW>

    % change scatter_point_size
    vh.ScatterPlot.SizeData = s.scatSize;
    vh.ScatterPlot.MarkerFaceColor = s.scatColor;
    vh.ScatterPlot.MarkerFaceAlpha = s.scatAlpha;
end

% significance bar
ast = sub_getStars(stats.motion_energy_balance.Af.kinematics_signedRank.pValue);
line([1 2],[0.93 0.93],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
text(1.5,0.935,ast,'HorizontalAlignment','center','FontSize',s.sigAstSize)

% finetuning
xlim([0.3 2.7]);
ylim([0.7 0.95]);
axB = gca;
axB.XAxis.TickLength = [0 0];
axB.YGrid = 'on';
set(axB,...
        'XTick',1:2,...
        'XTickLabelRotation',45,...
        'XTickLabel',{'correct','incorrect'},...
        'YTick',[.7 .8 .9],...
        'fontsize',s.axesFontSize);

% IPD --- ANOVA results ------------------------------------------------------------------
subplot(3,3,[4 5]); cla;
box off; hold on;

% get current data
plt_anova(:,1) = data.IP_distance_avg.An_t0 ./ 1000;    % convert mm -> m
plt_anova(:,2) = data.IP_distance_avg.An_t500 ./ 1000;
plt_anova(:,3) = data.IP_distance_avg.An_t1000 ./ 1000;
plt_anova(:,4) = data.IP_distance_avg.An_t2000 ./ 1000;

% violin plot
mxVal = -inf;
mnVal = inf;
for ts = 1:4
    vh = Violin(plt_anova(:,ts), ts,...
        'Width',s.violinWidth,...
        'ViolinColor',s.faceColor(ts,:),...
        'ViolinAlpha',s.alpha,...
        'BoxColor',s.boxColor,...
        'BoxWidth',s.boxWidth,...
        'MedianColor',s.medianColor,...
        'ShowMean',true); %#ok<*SAGROW>

    % change scatter_point_size
    vh.ScatterPlot.SizeData = s.scatSize;
    vh.ScatterPlot.MarkerFaceColor = s.scatColor;
    vh.ScatterPlot.MarkerFaceAlpha = s.scatAlpha;

    % save min/max value for sigLines
    mxVal = max([mxVal; plt_anova(:,ts)]);
    mnVal = min([mnVal; plt_anova(:,ts)]);
end

% plotting_settings depending on actual data
s.sigLineFirstInt = (mxVal - mnVal) * .10;
s.sigLineInt = (mxVal - mnVal) * .05;
s.sigLineStarDist = .05;
s.sigLineStarYOffset = -(mxVal - mnVal) * .01;

% significance bars
sigN = sum(stats.IP_distance_avg.An.posthoc.pValue < 0.05)/2;
if sigN > 0
    sigLineMaxVal = mxVal;
    sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
    sigLineI = 1;
    for i = 1:3
        for j = 4:-1:i+1
            cmp_ind = contains(stats.IP_distance_avg.An.posthoc.TimeShift_1,orderTimeShifts{i}) & contains(stats.IP_distance_avg.An.posthoc.TimeShift_2,orderTimeShifts{j});
            cmp_pVal = stats.IP_distance_avg.An.posthoc.pValue(cmp_ind);

            if cmp_pVal < 0.05
                ast = sub_getStars(cmp_pVal);
                line([i j],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                text(j + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
                sigLineI = sigLineI + 1;
            end
        end
    end
end

% finetuning
xlim([0.3 4.7]);
ylim([0.4 2]);
axC = gca;
axC.XAxis.TickLength = [0 0];
axC.YGrid = 'on';
set(axC,'XTick',[],...
        'YTick',[0.5 1 1.5 2],...
        'fontsize',s.axesFontSize);
ylabel('IPDavg [m]','FontSize',s.txtSizeYLabel);

% PS --- ANOVA results -------------------------------------------------------------------
subplot(3,3,[7 8]); cla;
box off; hold on;

% get current data
plt_anova(:,1) = data.personalspace.An_t0;
plt_anova(:,2) = data.personalspace.An_t500;
plt_anova(:,3) = data.personalspace.An_t1000;
plt_anova(:,4) = data.personalspace.An_t2000;

% violin plot
mxVal = -inf;
mnVal = inf;
for ts = 1:4
    vh = Violin(plt_anova(:,ts), ts,...
        'Width',s.violinWidth,...
        'ViolinColor',s.faceColor(ts,:),...
        'ViolinAlpha',s.alpha,...
        'BoxColor',s.boxColor,...
        'BoxWidth',s.boxWidth,...
        'MedianColor',s.medianColor,...
        'ShowMean',true); %#ok<*SAGROW>

    % change scatter_point_size
    vh.ScatterPlot.SizeData = s.scatSize;
    vh.ScatterPlot.MarkerFaceColor = s.scatColor;
    vh.ScatterPlot.MarkerFaceAlpha = s.scatAlpha;

    % save min/max value for sigLines
    mxVal = max([mxVal; plt_anova(:,ts)]);
    mnVal = min([mnVal; plt_anova(:,ts)]);
end

% plotting_settings depending on actual data
s.sigLineFirstInt = (mxVal - mnVal) * .10;
s.sigLineInt = (mxVal - mnVal) * .05;
s.sigLineStarDist = .05;
s.sigLineStarYOffset = -(mxVal - mnVal) * .01;

% significance bars
sigN = sum(stats.personalspace.An.posthoc.pValue < 0.05)/2;
if sigN > 0
    sigLineMaxVal = mxVal;
    sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
    sigLineI = 1;
    for i = 1:3
        for j = 4:-1:i+1
            cmp_ind = contains(stats.personalspace.An.posthoc.TimeShift_1,orderTimeShifts{i}) & contains(stats.personalspace.An.posthoc.TimeShift_2,orderTimeShifts{j});
            cmp_pVal = stats.personalspace.An.posthoc.pValue(cmp_ind);

            if cmp_pVal < 0.05
                ast = sub_getStars(cmp_pVal);
                line([i j],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                text(j + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'FontSize',s.sigAstSize)
                sigLineI = sigLineI + 1;
            end
        end
    end
end

% finetuning
xlim([0.3 4.7]);
ylim([0 113]);
axE = gca;
axE.XAxis.TickLength = [0 0];
axE.YGrid = 'on';
set(axE,...
    'XTick',1:4,...
    'XTickLabel',txt.timeshift,...
    'XTickLabelRotation',45,...
    'YTick',[0 25 50 75 100],...
    'fontsize',s.axesFontSize)
ylabel('PS [%]','FontSize',s.txtSizeYLabel);

% figure-finetuning ----------------------------------------------------------------------
% add A) ... D)
annotation('textbox',[.02 .88 .1 .1],'String','A)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.61 .88 .1 .1],'String','D)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.02 .59 .1 .1],'String','B)','FontSize',14,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.02 .29 .1 .1],'String','C)','FontSize',14,'FontWeight','bold','EdgeColor','none');

% ----------------------------------------------------------------------------------------
%% figure *** supplementary S3: ANOVA -----------------------------------------------------
% ----------------------------------------------------------------------------------------
disp('  --> plotting supplementary figure S3 (rmANOVA results)');

% update settings
s.txtSizeYLabel = 10;
s.axesFontSize = 8;
s.sigAstSize = 12;

% init figure
fh_supp = figure();
set(fh_supp,'Position',[100 10 900 1100]);
set(fh_supp,'Color',s.background_color);

fh_m = nFeat;
fh_n = 2;
fh_i = 1;

% loop features ----------------------------------------------------------------------
for iFeat = 1:nFeat
    % get feature index
    f = find(contains(features,txt.features(iFeat,1)));

    % loop emotions
    for e = eoi
        % ANOVA results ------------------------------------------------------------------
        subplot(fh_m,fh_n,fh_i); cla;
        box off; hold on;

        % get current data
        plt_anova(:,1) = data.(features{f}).([e{1} '_t0']);
        plt_anova(:,2) = data.(features{f}).([e{1} '_t500']);
        plt_anova(:,3) = data.(features{f}).([e{1} '_t1000']);
        plt_anova(:,4) = data.(features{f}).([e{1} '_t2000']);

        % violin plot
        mxVal = -inf;
        mnVal = inf;
        for ts = 1:4
            vh = Violin(plt_anova(:,ts), ts,...
                'Width',s.violinWidth,...
                'ViolinColor',s.faceColor(ts,:),...
                'ViolinAlpha',s.alpha,...
                'BoxColor',s.boxColor,...
                'BoxWidth',s.boxWidth,...
                'MedianColor',s.medianColor,...
                'ShowMean',true); %#ok<*SAGROW>

            % change scatter_point_size
            vh.ScatterPlot.SizeData = s.scatSize;
            vh.ScatterPlot.MarkerFaceColor = s.scatColor;
            vh.ScatterPlot.MarkerFaceAlpha = s.scatAlpha;

            % save min/max value for sigLines
            mxVal = max([mxVal; plt_anova(:,ts)]);
            mnVal = min([mnVal; plt_anova(:,ts)]);
        end

        % plotting_settings depending on actual data
        s.sigLineFirstInt = (mxVal - mnVal) * .10;
        s.sigLineInt = (mxVal - mnVal) * .05;
        s.sigLineStarDist = .05;
        s.sigLineStarYOffset = -(mxVal - mnVal) * .01;

        % significance bars
        sigN = sum(stats.(features{f}).(e{1}).posthoc.pValue < 0.05)/2;
        if sigN > 0
            sigLineMaxVal = mxVal;
            sigLineHeight = flip(linspace(sigLineMaxVal+s.sigLineFirstInt,sigLineMaxVal+s.sigLineFirstInt+sigN*s.sigLineInt-s.sigLineInt,sigN));
            sigLineI = 1;
            for i = 1:3
                for j = 4:-1:i+1
                    cmp_ind = contains(stats.(features{f}).(e{1}).posthoc.TimeShift_1,orderTimeShifts{i}) & contains(stats.(features{f}).(e{1}).posthoc.TimeShift_2,orderTimeShifts{j});
                    cmp_pVal = stats.(features{f}).(e{1}).posthoc.pValue(cmp_ind);

                    if cmp_pVal < 0.05
                        ast = sub_getStars(cmp_pVal);
                        line([i j],[sigLineHeight(sigLineI) sigLineHeight(sigLineI)],'Color',s.sigLineCol,'LineWidth',s.sigLineWidth);
                        text(j + s.sigLineStarDist,sigLineHeight(sigLineI)+s.sigLineStarYOffset,ast,'fontSize',s.sigAstSize)
                        sigLineI = sigLineI + 1;
                    end
                end
            end
        end

        % finetuning
        xlim([0.3 4.7]);
        ax.(e{1}) = gca;
        ax.(e{1}).XAxis.TickLength = [0 0];
        ax.(e{1}).YGrid = 'on';
        if strcmp(e{1},eoi{1})      % add ylabel
            ylabel({strrep(txt.features{iFeat,2},'_','\_'),['[ ' units{f} ' ]']},'FontSize',s.txtSizeYLabel);
        end
        if iFeat == 12              % add x tick labels
            set(ax.(e{1}),...
                'XTick',1:4,...
                'XTickLabel',txt.timeshift);
        else
            set(ax.(e{1}),...
                'XTick',[],...
                'XTickLabel',txt.timeshift);
        end
        if numel(ax.(e{1}).YTick) == 2	% add minor ticks
            tmpNT = linspace(ax.(e{1}).YTick(1), ax.(e{1}).YTick(2), 3);
            if ax.(e{1}).YLim(1) < (tmpNT(1) - diff(tmpNT(1:2)))
                tmpNT = [(tmpNT(1) - diff(tmpNT(1:2))) tmpNT]; %#ok<AGROW>
            end
            if ax.(e{1}).YLim(2) > (tmpNT(end) + diff(tmpNT(1:2)))
                tmpNT = [tmpNT (tmpNT(end) + diff(tmpNT(1:2)))]; %#ok<AGROW>
            end
            set(ax.(e{1}),'YTick',tmpNT);
        end

        % write significant ANOVA results into subplot
        plt_txt.F = stats.(features{f}).(e{1}).ranovatable.F(1);
        switch stats.(features{f}).(e{1}).ranovatable.correction{1}
            case 'none'
                plt_txt.df1 = sprintf('%d',stats.(features{f}).(e{1}).ranovatable.DF(1));
                plt_txt.df2 = sprintf('%d',stats.(features{f}).(e{1}).ranovatable.DF(2));
                plt_txt.p = stats.(features{f}).(e{1}).ranovatable.pValue(1);
            case 'GG'
                plt_txt.df1 = sprintf('%.2f',stats.(features{f}).(e{1}).ranovatable.dfGG(1));
                plt_txt.df2 = sprintf('%.2f',stats.(features{f}).(e{1}).ranovatable.dfGG(2));
                plt_txt.p = stats.(features{f}).(e{1}).ranovatable.pValueGG(1);
            case 'HF'
                plt_txt.df1 = sprintf('%.2f',stats.(features{f}).(e{1}).ranovatable.dfHF(1));
                plt_txt.df2 = sprintf('%.2f',stats.(features{f}).(e{1}).ranovatable.dfHF(2));
                plt_txt.p = stats.(features{f}).(e{1}).ranovatable.pValueHF(1);
        end
        plt_txt.pEtaSq = stats.(features{f}).(e{1}).ranovatable.pEtaSq(1);
        plt_txt.txt = sprintf('F(%s,%s) = %.2f, %s, \\eta^2_p = %.2f', plt_txt.df1,plt_txt.df2,plt_txt.F,sub_getPString(plt_txt.p),plt_txt.pEtaSq);

        if plt_txt.p < 0.05
            text(ax.(e{1}).XLim(1),ax.(e{1}).YLim(2) + range(ax.(e{1}).YLim)*.1,...
                plt_txt.txt,...
                'HorizontalAlignment','left',...
                'FontSize',s.txtSizeAnova);
        end

        % update subplot_index -----------------------------------------------------------
        fh_i = fh_i + 1;
    end %emotion loop
end % feature loop

% figure-finetuning ----------------------------------------------------------------------
% add A) + B)
annotation('textbox',[.05 .86 .1 .1],'String','A)','FontSize',12,'FontWeight','bold','EdgeColor','none');
annotation('textbox',[.51 .86 .1 .1],'String','B)','FontSize',12,'FontWeight','bold','EdgeColor','none');

%% ****************** save data and figures **********************************************
if saving_data == true
    disp('  --> saving analysis results');
    
    % loop emotions and then features ----------------------------------------------------
    for e = eoi
        for f = 1:nFeat
            % base_filename
            act_feat_idx = contains(txt.features(:,1), features{f});
            if strcmp(e{1},'Af')
                bfn = ['AnalysisD_' eoi_full{1} '_' txt.features{act_feat_idx,2} '_'];
            else
                bfn = ['AnalysisD_' eoi_full{2} '_' txt.features{act_feat_idx,2} '_'];
            end
            
            % get stats for this feature + emotion
            act_stats = stats.(features{f}).(e{1});
            
            % get current data
            act_data.t0 = data.(features{f}).([e{1} '_t0']);
            act_data.t500 = data.(features{f}).([e{1} '_t500']);
            act_data.t1000 = data.(features{f}).([e{1} '_t1000']);
            act_data.t2000 = data.(features{f}).([e{1} '_t2000']);
            
            % convert class of specific table_coloumns (don't know why, but neccessary)
            act_stats.ranovatable.F = double(act_stats.ranovatable.F);
            act_stats.ranovatable.pValue = double(act_stats.ranovatable.pValue);
            act_stats.ranovatable.pValueGG = double(act_stats.ranovatable.pValueGG);
            act_stats.ranovatable.pValueHF = double(act_stats.ranovatable.pValueHF);
            act_stats.ranovatable.pValueLB = double(act_stats.ranovatable.pValueLB);
            
            % save tables
            writetable(struct2table(act_data),fullfile(folder.data,[bfn 'kinematic_data_for_sequences_by_temporalOffset.csv']));
            act_stats.feat_sw.Properties.DimensionNames{1} = 'condition';
            writetable(act_stats.feat_sw,fullfile(folder.data,[bfn 'Shapiro_Wilks.csv']),'WriteRowNames',true);
            writetable(act_stats.ranovatable,fullfile(folder.data,[bfn 'stats_anova.csv']),'WriteRowNames',true);
            writetable(act_stats.posthoc,fullfile(folder.data,[bfn 'stats_postHoc.csv']));
            
            writetable(struct2table(act_stats.kinematics),fullfile(folder.data,[bfn 'kinematic_data_for_subjects_by_recognition.csv']));
            writetable(struct2table(act_stats.kinematics_signedRank),fullfile(folder.data,[bfn 'stats_signedRank.csv']));
        end
    end
end

if saving_figs == true
    disp('  --> saving figures');
    
    savefig(fh_kin,fullfile(folder.figs,'AnalysisD_fig6_kinematic_analysis'));
    exportgraphics(fh_kin,fullfile(folder.figs,'AnalysisD_fig6_kinematic_analysis.pdf'),'ContentType','vector');
    
    savefig(fh_supp,fullfile(folder.figs,'AnalysisD_figS3_kinematics_by_temporalOffset'));
    exportgraphics(fh_supp,fullfile(folder.figs,'AnalysisD_figS3_kinematics_by_temporalOffset.pdf'),'ContentType','vector');
end

%% ****************** displaying anova results *******************************
disp('  --> displaying statistical results');
% loop emotions and then features ----------------------------------------------------
for e = 1:2
    for f = 1:nFeat
        act_feat_idx = contains(txt.features(:,1), features{f});
        disp(['     --> testing ''' txt.features{act_feat_idx,2} ''' in ''' eoi_full{e} ''' sequences ---------------------------------------']);
        disp(' ');

        % prepare stats
        switch stats.(features{f}).(eoi{e}).ranovatable.correction{1}
            case 'none'
                this_correction = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f',...
                                                stats.(features{f}).(eoi{e}).ranovatable.mauchlyDF(1),...
                                                stats.(features{f}).(eoi{e}).ranovatable.mauchlyChiSq(1),...
                                                sub_getPString(stats.(features{f}).(eoi{e}).ranovatable.mauchlyPvalue(1)),...
                                                stats.(features{f}).(eoi{e}).ranovatable.epsilonGG(1));

                this_df1 = sprintf('%d',stats.(features{f}).(eoi{e}).ranovatable.DF(1));
                this_df2 = sprintf('%d',stats.(features{f}).(eoi{e}).ranovatable.DF(2));
                this_F = stats.(features{f}).(eoi{e}).ranovatable.F(1);
                this_p = stats.(features{f}).(eoi{e}).ranovatable.pValue(1);
                this_pETAsq = stats.(features{f}).(eoi{e}).ranovatable.pEtaSq(1);

            case 'GG'
                this_correction = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Greenhouse-Geisser correction',...
                                                stats.(features{f}).(eoi{e}).ranovatable.mauchlyDF(1),...
                                                stats.(features{f}).(eoi{e}).ranovatable.mauchlyChiSq(1),...
                                                sub_getPString(stats.(features{f}).(eoi{e}).ranovatable.mauchlyPvalue(1)),...
                                                stats.(features{f}).(eoi{e}).ranovatable.epsilonGG(1));

                this_df1 = sprintf('%.2f',stats.(features{f}).(eoi{e}).ranovatable.dfGG(1));
                this_df2 = sprintf('%.2f',stats.(features{f}).(eoi{e}).ranovatable.dfGG(2));
                this_F = stats.(features{f}).(eoi{e}).ranovatable.F(1);
                this_p = stats.(features{f}).(eoi{e}).ranovatable.pValueGG(1);
                this_pETAsq = stats.(features{f}).(eoi{e}).ranovatable.pEtaSq(1);

            case 'HF'
                this_correction = sprintf('Mauchly: chi^2(%d) = %.2f, %s, epsilon = %.3f -> Huyn-Feldt correction',...
                                                stats.(features{f}).(eoi{e}).ranovatable.mauchlyDF(1),...
                                                stats.(features{f}).(eoi{e}).ranovatable.mauchlyChiSq(1),...
                                                sub_getPString(stats.(features{f}).(eoi{e}).ranovatable.mauchlyPvalue(1)),...
                                                stats.(features{f}).(eoi{e}).ranovatable.epsilonGG(1));

                this_df1 = sprintf('%.2f',stats.(features{f}).(eoi{e}).ranovatable.dfHF(1));
                this_df2 = sprintf('%.2f',stats.(features{f}).(eoi{e}).ranovatable.dfHF(2));
                this_F = stats.(features{f}).(eoi{e}).ranovatable.F(1);
                this_p = stats.(features{f}).(eoi{e}).ranovatable.pValueHF(1);
                this_pETAsq = stats.(features{f}).(eoi{e}).ranovatable.pEtaSq(1);
        end

        this_sr_Z = stats.(features{f}).(eoi{e}).kinematics_signedRank.Z;
        this_sr_p = stats.(features{f}).(eoi{e}).kinematics_signedRank.pValue;
        this_sr_r = stats.(features{f}).(eoi{e}).kinematics_signedRank.r;

        % display stats
        disp('         --- ANOVA results:');
        fprintf('             main effect -> Temporal Offset\n\t\t\t\t%s\n\t\t\t\tF(%s,%s) = %.3f, %s, partial Eta^2 = %.3f\n',...
            this_correction,this_df1,this_df2,this_F,sub_getPString(this_p),this_pETAsq);
        disp(' ');
        disp('         --- Wilcoxon signed rank test results: ');
        fprintf('             correct vs incorrect recognized sequences\n\t\t\t\tZ = %.3f, %s, r = %.3f\n',...
            this_sr_Z,sub_getPString(this_sr_p),this_sr_r);
        disp(' ');

    end
end

%% finished
disp(' ');
disp('   >>> done <<<');
