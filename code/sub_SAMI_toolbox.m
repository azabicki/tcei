function [feat,c3dData] = sub_SAMI_toolbox(folder)
% addpath
addpath(genpath(folder.sami_toolbox));

% obligatory: loading userOptions / initializing SAMItoolbox +++++++++++++++++++++++++++++
userOptions = sub_SAMI_userOptions(folder);
userOptions = sami.initSAMI(userOptions,'c'); % or 'd' for deleting all files

% +++ loading c3d-files and checking them ++++++++++++++++++++++++++++++++++++++++++++
c3dData = sami.c3d.importFiles(userOptions);

% +++ calculate features +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
feat.Af_t0    = sami.calcFeatures(c3dData(1:4:68), 'itx', userOptions, '_Af_t0');
feat.Af_t500  = sami.calcFeatures(c3dData(2:4:68), 'itx', userOptions, '_Af_t500');
feat.Af_t1000 = sami.calcFeatures(c3dData(3:4:68), 'itx', userOptions, '_Af_t1000');
feat.Af_t2000 = sami.calcFeatures(c3dData(4:4:68), 'itx', userOptions, '_Af_t2000');

feat.An_t0    = sami.calcFeatures(c3dData(69:4:136), 'itx', userOptions, '_An_t0');
feat.An_t500  = sami.calcFeatures(c3dData(70:4:136), 'itx', userOptions, '_An_t500');
feat.An_t1000 = sami.calcFeatures(c3dData(71:4:136), 'itx', userOptions, '_An_t1000');
feat.An_t2000 = sami.calcFeatures(c3dData(72:4:136), 'itx', userOptions, '_An_t2000');

feat.Ha_t0    = sami.calcFeatures(c3dData(137:4:204), 'itx', userOptions, '_Ha_t0');
feat.Ha_t500  = sami.calcFeatures(c3dData(138:4:204), 'itx', userOptions, '_Ha_t500');
feat.Ha_t1000 = sami.calcFeatures(c3dData(139:4:204), 'itx', userOptions, '_Ha_t1000');
feat.Ha_t2000 = sami.calcFeatures(c3dData(140:4:204), 'itx', userOptions, '_Ha_t2000');

feat.Sa_t0    = sami.calcFeatures(c3dData(205:4:272), 'itx', userOptions, '_Sa_t0');
feat.Sa_t500  = sami.calcFeatures(c3dData(206:4:272), 'itx', userOptions, '_Sa_t500');
feat.Sa_t1000 = sami.calcFeatures(c3dData(207:4:272), 'itx', userOptions, '_Sa_t1000');
feat.Sa_t2000 = sami.calcFeatures(c3dData(208:4:272), 'itx', userOptions, '_Sa_t2000');
end