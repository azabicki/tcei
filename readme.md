# Temporal Coupling of Emotional Interactions

Here you find all relevant data, code, and useful links regarding this project.

## reading material
*OSF public registrations*. It contains, in a frozen image of the GitLab repository, all data and code used to calculate findings as reported:

- Bachmann, J., Krüger, B., Munzert, J., & Zabicki, A. (2020, December 3). _Temporal Coupling in Emotional Interactions._ [DOI: 10.17605/OSF.IO/4M3XE](https://doi.org/10.17605/OSF.IO/4M3XE)
  - version 1: december 2020


- Bachmann, J., Krüger, B., Keck, J., Munzert, J., & Zabicki, A. (2021, october 4). _Temporal Coupling in Emotional Interactions._ [DOI: 10.17605/OSF.IO/Z3T4J](https://doi.org/10.17605/OSF.IO/Z3T4J)  
  - version 2: october 2021


*preprint:*

- Bachmann, J., Krüger, B., Munzert, J. & Zabicki, A. (2020) _When the Timing is Right: The Link Between Temporal Coupling in Dyadic Interactions and Emotion Recognition._ PsyArXiv [DOI: 10.31234/osf.io/rkg3j](https://doi.org/10.31234/osf.io/rkg3j)
  - version 1: december 2020
  - version 2: october 2021

*peer-reviewed:*
- Bachmann, J., Krüger, B., Keck, J., Munzert, J., & Zabicki, A. (2022). When the timing is right: The link between temporal coupling in dyadic interactions and emotion recognition. Cognition, 229, 105267. https://doi.org/10.1016/j.cognition.2022.105267

## data/code repository

This Gitlab repository ([gitlab.com/azabicki/tcei](https://gitlab.com/azabicki/tcei)) contains all relevant data and code to reproduce our findings in the following folders:

- analysis_figures
  - contains all created figures as *.tiff as well as MATLAB *.fig files
- analysis_results
  - contains results, i.e. all statistical data, calculated during execution of MATLAB analysis scripts  
- analysis_results/SAMI_results
  - explicit folder for results obtained from calculations by SAMI toolbox  
- code
  - contains MATLAB code to reproduce our findings
- raw_exp_data
  - contains raw experimental data of participants as *.txt files
- SAMI-0.1.0
  - contains SAMI toolbox (version v0.1.0), which is also archived in Zenodo. [DOI: 10.5281/zenodo.4764552](https://doi.org/10.5281/zenodo.4764552)
- stimuli
  - contains stimulus set used in this experiment as *.avi files
- stimuli_c3d
    - contains *.c3d files of stimuli used in this experiment, needed for kinematic analysis by SAMI

## analysis pipeline

run the following MATLAB scripts to reproduce our findings.

**Analysis A:**

comparing emotion category ratings of baseline sequences (first + 0ms trials) with external validated emotional categories

what happens here:
- create confusion-matrix
- test recognition rates of each emotion against chance
- investigate influence of BDI2/STAI-scores on recognition rate via spearman correlation analysis

code that leads to
- figure 2 in the manuscript
- figure A.1 in the Appendix A
- statistical values (see output in command window)

**Analysis B:**

investigating influence of temporal offset on emotion recognition by means of comparing emotion category ratings of temporally decoupled sequences with individual baseline sequences

code that leads to
- figures 3 and 4 in the manuscript
- statistical values (see output in command window)

**Analysis C:**

investigating influence of temporal offset on valence perception by means of comparing absolute valence ratings of temporally decoupled sequences

code that leads to
- figure 5 in the manuscript
- figure A.2 in the Appendix A, showing distribution of actual valence ratings with respect to validated emotion categories (not based on baseline-emotion as in fig 5)
- statistical values (see output in command window)

**Analysis D:**

exploratory analysis, investigating influence of temporal offset on kinematic parameters obtained by 'SAMI' toolbox, and testing associations with perceived emotion ratings of temporally decoupled sequences

code that leads to
- figure 6 in the manuscript
- figure A.3 in Appendix A
- statistical values (see output in command window), shown in table A.5 in Appendix A

## stimulus set

In the _stimuli_ folder (or _stimuli_c3d_ folder)  you will find the complete stimulus set used in the present study as \*.avi files (or \*.c3d files).

For each individual interacting scene, four different video files exists. The original (+ 0ms) interaction, as well as three temporally decoupled variations (+ 500ms, + 1000ms and + 2000ms).

Filenames are coded as following:
- for unshifted interactions
  - [stimulusCode].avi
- for temporally shifted interactions
  - [stimulusCode]\_[timeShift]\_[personFixInTime].avi

with:
- [stimulusCode] = unique identifier for each interaction
- [timeShift] = how long (in ms) the actions of one person are shifted in time
- [personFixInTime] = which of both actors was fixed in time (i.e. the other actor was shifted)
